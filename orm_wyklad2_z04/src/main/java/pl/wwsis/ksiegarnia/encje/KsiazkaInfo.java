package pl.wwsis.ksiegarnia.encje;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ORM
 *
 */
@Embeddable
public class KsiazkaInfo implements Serializable {
	@Column(name = "OPIS")
	private String opis;

	@Column(name = "DATA_WYDANIA")
	@Temporal(TemporalType.DATE)
	private Date dataWydania;

	@Column(name = "OSTATNIA_MODYFIKACJA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date ostatniaModyfikacja;

	@Column(name = "KATEGORIA")
	@Enumerated(EnumType.STRING)
	private Kategoria kategoria;

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Date getDataWydania() {
		return dataWydania;
	}

	public void setDataWydania(Date dataWydania) {
		this.dataWydania = dataWydania;
	}

	public Date getOstatniaModyfikacja() {
		return ostatniaModyfikacja;
	}

	public void setOstatniaModyfikacja(Date ostatniaModyfikacja) {
		this.ostatniaModyfikacja = ostatniaModyfikacja;
	}

	public Kategoria getKategoria() {
		return kategoria;
	}

	public void setKategoria(Kategoria kategoria) {
		this.kategoria = kategoria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataWydania == null) ? 0 : dataWydania.hashCode());
		result = prime * result + ((kategoria == null) ? 0 : kategoria.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((ostatniaModyfikacja == null) ? 0 : ostatniaModyfikacja.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KsiazkaInfo other = (KsiazkaInfo) obj;
		if (dataWydania == null) {
			if (other.dataWydania != null)
				return false;
		} else if (!dataWydania.equals(other.dataWydania))
			return false;
		if (kategoria != other.kategoria)
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		if (ostatniaModyfikacja == null) {
			if (other.ostatniaModyfikacja != null)
				return false;
		} else if (!ostatniaModyfikacja.equals(other.ostatniaModyfikacja))
			return false;
		return true;
	}

}
