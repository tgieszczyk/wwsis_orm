package pl.wwsis.ksiegarnia.encje;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author ORM
 *
 */
@Entity
@Table(name = "KSIAZKA")
public class Ksiazka implements Serializable {
	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "generator_id_ksiazka", sequenceName = "SEQ_KSIAZKA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id_ksiazka")
	private Long id;

	@Column(name = "TYTUL")
	private String tytul;

	@Transient
	private Autor autor;

	@Embedded
	private KsiazkaInfo informacjePodstawowe;

	// kazde duze dany oznaczamy jako @Lob
	@Column(name = "ZDJECIE")
	@Lob
	// Kazde duze dane powinnismy ladowac wtedy kiedy sa one nam potrzebne czyli
	// fetch = FetchType.LAZY
	@Basic(fetch = FetchType.LAZY)
	private byte[] zdjecie;

	@Column(name = "CENA_NETTO")
	private double cenaNetto;

	@Column(name = "ID_AUTORA")
	private Long autorId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	// nasze lazy initialization -> tworzymy obiekt wtedy kiedy jest potrzebny
	public KsiazkaInfo getInformacjePodstawowe() {
		if (informacjePodstawowe == null) {
			informacjePodstawowe = new KsiazkaInfo();
		}
		return informacjePodstawowe;
	}

	public void setInformacjePodstawowe(KsiazkaInfo informacjePodstawowe) {
		this.informacjePodstawowe = informacjePodstawowe;
	}

	public byte[] getZdjecie() {
		return zdjecie;
	}

	public void setZdjecie(byte[] zdjecie) {
		this.zdjecie = zdjecie;
	}

	public double getCenaNetto() {
		return cenaNetto;
	}

	public void setCenaNetto(double cenaNetto) {
		this.cenaNetto = cenaNetto;
	}

	public Long getAutorId() {
		return autorId;
	}

	public void setAutorId(Long autorId) {
		this.autorId = autorId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autor == null) ? 0 : autor.hashCode());
		result = prime * result + (int) (autorId ^ (autorId >>> 32));
		long temp;
		temp = Double.doubleToLongBits(cenaNetto);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((informacjePodstawowe == null) ? 0 : informacjePodstawowe.hashCode());
		result = prime * result + ((tytul == null) ? 0 : tytul.hashCode());
		result = prime * result + Arrays.hashCode(zdjecie);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ksiazka other = (Ksiazka) obj;
		if (autor == null) {
			if (other.autor != null)
				return false;
		} else if (!autor.equals(other.autor))
			return false;
		if (autorId != other.autorId)
			return false;
		if (Double.doubleToLongBits(cenaNetto) != Double.doubleToLongBits(other.cenaNetto))
			return false;
		if (id != other.id)
			return false;
		if (informacjePodstawowe == null) {
			if (other.informacjePodstawowe != null)
				return false;
		} else if (!informacjePodstawowe.equals(other.informacjePodstawowe))
			return false;
		if (tytul == null) {
			if (other.tytul != null)
				return false;
		} else if (!tytul.equals(other.tytul))
			return false;
		if (!Arrays.equals(zdjecie, other.zdjecie))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
