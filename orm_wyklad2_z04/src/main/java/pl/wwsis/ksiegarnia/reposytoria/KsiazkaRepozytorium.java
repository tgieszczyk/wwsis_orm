package pl.wwsis.ksiegarnia.reposytoria;

import java.util.List;

import javax.persistence.EntityManager;

import pl.wwsis.ksiegarnia.EntityManagerThreadContext;
import pl.wwsis.ksiegarnia.encje.Ksiazka;

/**
 * @author ORM
 *
 */
// warstwa dostepu do danych 
public class KsiazkaRepozytorium {
	private static KsiazkaRepozytorium JEDNA_INSTANCJA = new KsiazkaRepozytorium();

	public static KsiazkaRepozytorium getInstance() {
		return JEDNA_INSTANCJA;
	}

	@SuppressWarnings("unchecked")
	public List<Ksiazka> znajdzWszystkieKsiazki() {
		EntityManager em = EntityManagerThreadContext.getInstance().getEntityManager();
		// Pierwszy JPQL - czyli zapytania do encji nie do tabel, to encje maja
		// informacje o tabelach i kolumnach
		return (List<Ksiazka>) em.createQuery("from Ksiazka").getResultList();
	}
	
	public void saveOrUpdate(Ksiazka k) {
		EntityManager em = EntityManagerThreadContext.getInstance().getEntityManager();
		if (k.getId() != null) {
			// juz istnieje w bazie danych - uaktualniamy
			em.merge(k);
		} else {
			// jeszcze nie ma ksiazki, dodajemy do bazy danych
			em.persist(k);
		}
	}
}
