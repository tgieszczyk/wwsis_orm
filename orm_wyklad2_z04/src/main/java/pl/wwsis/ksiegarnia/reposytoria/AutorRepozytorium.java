package pl.wwsis.ksiegarnia.reposytoria;

import javax.persistence.EntityManager;

import pl.wwsis.ksiegarnia.EntityManagerThreadContext;
import pl.wwsis.ksiegarnia.encje.Autor;

/**
 * @author ORM
 *
 */
public class AutorRepozytorium {
	private static AutorRepozytorium JEDNA_INSTANCJA = new AutorRepozytorium();

	public static AutorRepozytorium getInstance() {
		return JEDNA_INSTANCJA;
	}

	public Autor znajdAutoraPoKluczu(Long autorId) {
		EntityManager em = EntityManagerThreadContext.getInstance().getEntityManager();
		return em.find(Autor.class, autorId);
	}

	public void saveOrUpdate(Autor a) {
		EntityManager em = EntityManagerThreadContext.getInstance().getEntityManager();
		if (a.getId() != null) {
			// juz istnieje w bazie danych - uaktualniamy
			em.merge(a);
		} else {
			// jeszcze nie ma ksiazki, dodajemy do bazy danych
			em.persist(a);
		}
	}
}
