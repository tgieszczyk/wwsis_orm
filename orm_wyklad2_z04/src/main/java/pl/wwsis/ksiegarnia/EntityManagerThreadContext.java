package pl.wwsis.ksiegarnia;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author ORM
 *
 */
public class EntityManagerThreadContext {
	
	// wzorzec projektowy singleton
	private static EntityManagerThreadContext JEDNA_INSTANCJA = new EntityManagerThreadContext();

	public static EntityManagerThreadContext getInstance() {
		return JEDNA_INSTANCJA;
	}

	// wzorzez projektowy ThreadLocal
	private ThreadLocal<EntityManager> entityManagerInstance = new ThreadLocal<EntityManager>();

	public EntityManager getEntityManager() {
		EntityManager result = entityManagerInstance.get();
		if (result == null) {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

			result = factory.createEntityManager();
			entityManagerInstance.set(result);
		}
		return result;
	}
}
