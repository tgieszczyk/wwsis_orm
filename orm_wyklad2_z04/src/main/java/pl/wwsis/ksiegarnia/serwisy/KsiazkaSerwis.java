package pl.wwsis.ksiegarnia.serwisy;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import pl.wwsis.ksiegarnia.EntityManagerThreadContext;
import pl.wwsis.ksiegarnia.encje.Autor;
import pl.wwsis.ksiegarnia.encje.Ksiazka;
import pl.wwsis.ksiegarnia.reposytoria.AutorRepozytorium;
import pl.wwsis.ksiegarnia.reposytoria.KsiazkaRepozytorium;

/**
 * @author ORM
 *
 */
public class KsiazkaSerwis {
	private static KsiazkaSerwis JEDNA_INSTANCJA = new KsiazkaSerwis();

	public static KsiazkaSerwis getInstance() {
		return JEDNA_INSTANCJA;
	}

	public List<Ksiazka> znajdzWszystkieKsiazki() {
		KsiazkaRepozytorium ksiazkaRepozytorium = KsiazkaRepozytorium.getInstance();
		AutorRepozytorium autorRepozytorium = AutorRepozytorium.getInstance();

		List<Ksiazka> wszystkieKsiazki = ksiazkaRepozytorium.znajdzWszystkieKsiazki();
		for (Ksiazka ksiazka : wszystkieKsiazki) {
			Long autorId = ksiazka.getAutorId();

			Autor autor = autorRepozytorium.znajdAutoraPoKluczu(autorId);
			ksiazka.setAutor(autor);
		}

		return wszystkieKsiazki;
	}

	public void dodajKsiazke(Ksiazka k) {
		KsiazkaRepozytorium ksiazkaRepozytorium = KsiazkaRepozytorium.getInstance();
		AutorRepozytorium autorRepozytorium = AutorRepozytorium.getInstance();

		// musimy pamietac ze zapis / update wykonujemy wewnatrz transakcji
		EntityManager em = EntityManagerThreadContext.getInstance().getEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();

			Autor autor = k.getAutor();
			if (autor != null) {
				autorRepozytorium.saveOrUpdate(autor);
				k.setAutorId(autor.getId());
			}
			ksiazkaRepozytorium.saveOrUpdate(k);

			transaction.commit();
		} catch (Exception e) {
			// dodajemy rollback jezeli wystapil blad
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}

	}
}
