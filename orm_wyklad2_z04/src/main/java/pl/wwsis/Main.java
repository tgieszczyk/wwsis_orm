package pl.wwsis;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.ksiegarnia.encje.Autor;
import pl.wwsis.ksiegarnia.encje.Kategoria;
import pl.wwsis.ksiegarnia.encje.Ksiazka;
import pl.wwsis.ksiegarnia.serwisy.KsiazkaSerwis;

public class Main {

	public static void main(String[] args) {
		Autor autor = new Autor();
		autor.setImie("Tomasz, Lukasz");
		autor.setNazwisko("Gieszczyk");

		Ksiazka ksiazka = new Ksiazka();

		ksiazka.setAutor(autor);
		ksiazka.setCenaNetto(25.99D);
		ksiazka.setTytul("ORM by Tomasz Gieszczyk");
		ksiazka.getInformacjePodstawowe().setDataWydania(new Date());
		ksiazka.getInformacjePodstawowe().setKategoria(Kategoria.DRAMA);
		ksiazka.getInformacjePodstawowe().setOpis("Lorum de lum");
		ksiazka.getInformacjePodstawowe().setOstatniaModyfikacja(new Date());

		KsiazkaSerwis.getInstance().dodajKsiazke(ksiazka);

		List<Ksiazka> ksiazki = KsiazkaSerwis.getInstance().znajdzWszystkieKsiazki();
		System.out.println(ksiazki);
		System.exit(0);
	}

}
