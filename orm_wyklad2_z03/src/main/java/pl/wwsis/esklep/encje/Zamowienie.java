package pl.wwsis.esklep.encje;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ORM
 *
 */
@Entity
public class Zamowienie implements Serializable {
	@Id
	@TableGenerator(name = "generator_tabelowy", 
		table = "SEKWENCJA_ZAMOWIENIA", 
		pkColumnName = "NAZWA_SEKWENCJI", // nazwa kolumny klucza glownego
		pkColumnValue = "KOLEJNY_NUMER", // wartosc unikalna w kolumnie klucza glownego
		valueColumnName = "KOLEJNY_NUMER_VALUE", // kolejna wartosc id
		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator_tabelowy")
	private long id;

	@Column(name = "NUMER_ZAMOWIENIA")
	private String numerZamowienia;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_UTWORZENIA")
	private Date dataUtworznie;

	@Column(name = "NIP_ZAMAWIAJACEGO")
	private String nipZamawiajacego;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumerZamowienia() {
		return numerZamowienia;
	}

	public void setNumerZamowienia(String numerZamowienia) {
		this.numerZamowienia = numerZamowienia;
	}

	public Date getDataUtworznie() {
		return dataUtworznie;
	}

	public void setDataUtworznie(Date dataUtworznie) {
		this.dataUtworznie = dataUtworznie;
	}

	public String getNipZamawiajacego() {
		return nipZamawiajacego;
	}

	public void setNipZamawiajacego(String nipZamawiajacego) {
		this.nipZamawiajacego = nipZamawiajacego;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataUtworznie == null) ? 0 : dataUtworznie.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((nipZamawiajacego == null) ? 0 : nipZamawiajacego.hashCode());
		result = prime * result + ((numerZamowienia == null) ? 0 : numerZamowienia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zamowienie other = (Zamowienie) obj;
		if (dataUtworznie == null) {
			if (other.dataUtworznie != null)
				return false;
		} else if (!dataUtworznie.equals(other.dataUtworznie))
			return false;
		if (id != other.id)
			return false;
		if (nipZamawiajacego == null) {
			if (other.nipZamawiajacego != null)
				return false;
		} else if (!nipZamawiajacego.equals(other.nipZamawiajacego))
			return false;
		if (numerZamowienia == null) {
			if (other.numerZamowienia != null)
				return false;
		} else if (!numerZamowienia.equals(other.numerZamowienia))
			return false;
		return true;
	}

}
