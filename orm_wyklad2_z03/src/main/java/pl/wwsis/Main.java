package pl.wwsis;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.esklep.encje.Zamowienie;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();

		try {

			Zamowienie zamowienie = new Zamowienie();
			zamowienie.setDataUtworznie(new Date());
			zamowienie.setNipZamawiajacego("000-990-00-98");
			zamowienie.setNumerZamowienia("1234/2016");
			transaction.begin();
			em.persist(zamowienie);
			// wykonujemy operacje create, delete, update
			// em.persist(p);
			// em.remove
			// em.merge

			transaction.commit();
			System.exit(0);
		} catch (Exception e) {

			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}

}
