package pl.wwsis.ksiegarnia.encje;

/**
 * @author ORM
 *
 */
public enum Kategoria {
	KOMEDIA, KRYMINAL, THRILER, DRAMA, SCIFI, PRZYGODA, OBYCZAJOWY;
}
