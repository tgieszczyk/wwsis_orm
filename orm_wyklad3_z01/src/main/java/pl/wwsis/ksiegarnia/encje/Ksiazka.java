package pl.wwsis.ksiegarnia.encje;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Zamiana na duze litery Ctrl+Shift+X Zamiana na male litery Ctrl+Shift+Y
 * 
 * @author ORM
 *
 */
@Entity
@Table(name = "KSIAZKA")
@SecondaryTable(name = "KSIAZKA_INFORMACJE_DODATKOWE", pkJoinColumns = {
		@PrimaryKeyJoinColumn(referencedColumnName = "TYTUL", name = "TYTUL"),
		@PrimaryKeyJoinColumn(referencedColumnName = "ROK_WYDANIA", name = "ROK_WYDANIA") })
@IdClass(KsiazkaPK.class)
public class Ksiazka implements Serializable {
	@Id
	@Column(name = "TYTUL")
	private String tytul;
	@Id
	@Column(name = "ROK_WYDANIA")
	@Temporal(TemporalType.DATE)
	private Date rokWydania;

	@Column(name = "KATEGORIA")
	@Enumerated(EnumType.STRING)
	private Kategoria kategoria;

	// zadanie 2 z listy 3 -> dodajemy atrybut 'length'
	@Column(name = "OPIS", length = 255)
	private String opis;
	@Column(name = "AUTOR", table = "KSIAZKA_INFORMACJE_DODATKOWE")
	private String autor;
	@Column(name = "WYDAWNICTWO", table = "KSIAZKA_INFORMACJE_DODATKOWE")
	private String wydawnictwo;

	public Kategoria getKategoria() {
		return kategoria;
	}

	public void setKategoria(Kategoria kategoria) {
		this.kategoria = kategoria;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getWydawnictwo() {
		return wydawnictwo;
	}

	public void setWydawnictwo(String wydawnictwo) {
		this.wydawnictwo = wydawnictwo;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public Date getRokWydania() {
		return rokWydania;
	}

	public void setRokWydania(Date rokWydania) {
		this.rokWydania = rokWydania;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autor == null) ? 0 : autor.hashCode());
		result = prime * result + ((kategoria == null) ? 0 : kategoria.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((rokWydania == null) ? 0 : rokWydania.hashCode());
		result = prime * result + ((tytul == null) ? 0 : tytul.hashCode());
		result = prime * result + ((wydawnictwo == null) ? 0 : wydawnictwo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ksiazka other = (Ksiazka) obj;
		if (autor == null) {
			if (other.autor != null)
				return false;
		} else if (!autor.equals(other.autor))
			return false;
		if (kategoria != other.kategoria)
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		if (rokWydania == null) {
			if (other.rokWydania != null)
				return false;
		} else if (!rokWydania.equals(other.rokWydania))
			return false;
		if (tytul == null) {
			if (other.tytul != null)
				return false;
		} else if (!tytul.equals(other.tytul))
			return false;
		if (wydawnictwo == null) {
			if (other.wydawnictwo != null)
				return false;
		} else if (!wydawnictwo.equals(other.wydawnictwo))
			return false;
		return true;
	}
}
