package pl.wwsis.ksiegarnia.encje;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ORM
 *
 */
@Embeddable
public class KsiazkaPK implements Serializable {
	@Id
	@Column(name = "TYTUL")
	private String tytul;
	@Id
	@Column(name = "ROK_WYDANIA")
	@Temporal(TemporalType.DATE)
	private Date rokWydania;
	
	public String getTytul() {
		return tytul;
	}
	public void setTytul(String tytul) {
		this.tytul = tytul;
	}
	public Date getRokWydania() {
		return rokWydania;
	}
	public void setRokWydania(Date rokWydania) {
		this.rokWydania = rokWydania;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rokWydania == null) ? 0 : rokWydania.hashCode());
		result = prime * result + ((tytul == null) ? 0 : tytul.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KsiazkaPK other = (KsiazkaPK) obj;
		if (rokWydania == null) {
			if (other.rokWydania != null)
				return false;
		} else if (!rokWydania.equals(other.rokWydania))
			return false;
		if (tytul == null) {
			if (other.tytul != null)
				return false;
		} else if (!tytul.equals(other.tytul))
			return false;
		return true;
	}
	
}
