package pl.wwsis;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.ksiegarnia.encje.Kategoria;
import pl.wwsis.ksiegarnia.encje.Ksiazka;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		// jesteśmy gotowi aby tworzyć encję

		EntityTransaction transaction = em.getTransaction();

		try {

			transaction.begin();
			Ksiazka ksiazka = new Ksiazka();
			ksiazka.setAutor("Tomasz Gieszczyk");
			ksiazka.setOpis(generate255Chars());
			ksiazka.setWydawnictwo("TMG Software Tomasz Gieszczyk");
			ksiazka.setRokWydania(new Date());
			ksiazka.setTytul("ORM");
			ksiazka.setKategoria(Kategoria.DRAMA);
			
			em.persist(ksiazka);

			// wykonujemy operacje create, delete, update
			//em.persist(p);
			// em.remove
			// em.merge

			transaction.commit();
			System.exit(0);
		} catch (Exception e) {

			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}
	
	private static String generate255Chars() {
		StringBuilder result = new StringBuilder();
		
		for(int i=0; i<257; i++) {
			result.append('t');
		}
		
		return result.toString();
	}

}
