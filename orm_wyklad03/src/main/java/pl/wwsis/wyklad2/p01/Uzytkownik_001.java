package pl.wwsis.wyklad2.p01;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "uzytkownik_001")
public class Uzytkownik_001 {
	@Id
	@TableGenerator(name = "GENERATOR_TABELA_UZYTKOWNIK", 
		table = "SEQUENCE_TABLE_GENERATOR", 
		pkColumnName = "NAZWA_SEKWENCJI", 
		pkColumnValue = "UZYTKOWNIK_SEQ")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "GENERATOR_TABELA_UZYTKOWNIK")
	private long id;

	private String name;

	private String nip;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

}
