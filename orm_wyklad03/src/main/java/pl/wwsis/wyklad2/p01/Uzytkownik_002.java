package pl.wwsis.wyklad2.p01;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "uzytkownik_002")
public class Uzytkownik_002 {
	@Id
	@SequenceGenerator(name = "GENERATOR_SEQ", sequenceName = "SEKWENCJA_UZYTKOWNIK")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GENERATOR_SEQ")
	private long id;

	private String name;

	private String nip;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

}
