package pl.wwsis.wyklad2.p02;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class EncjaStudent {
	@EmbeddedId
	private KluczKompozytowyStudent id;

	public KluczKompozytowyStudent getId() {
		return id;
	}

	public void setId(KluczKompozytowyStudent id) {
		this.id = id;
	}
	
	
}
