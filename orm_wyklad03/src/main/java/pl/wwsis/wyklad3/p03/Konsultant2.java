package pl.wwsis.wyklad3.p03;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TABLE_PER_CLASS_KONSULTANT")
public class Konsultant2 extends Pracownik2 {
	private double stawkaZaDzien;

	public double getStawkaZaDzien() {
		return stawkaZaDzien;
	}

	public void setStawkaZaDzien(double stawkaZaDzien) {
		this.stawkaZaDzien = stawkaZaDzien;
	}
}
