package pl.wwsis.wyklad3.p03;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@Table(name = "TABLE_PER_CLASS_PRACOWNIK")
//@Entity
public class Pracownik2 {
	@Id
	private String idPracownika;
	private String imie;
	private String nazwisko;
	private Date odKiedy;
	private String email;

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getIdPracownika() {
		return idPracownika;
	}

	public void setIdPracownika(String idPracownika) {
		this.idPracownika = idPracownika;
	}

	public Date getOdKiedy() {
		return odKiedy;
	}

	public void setOdKiedy(Date odKiedy) {
		this.odKiedy = odKiedy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
