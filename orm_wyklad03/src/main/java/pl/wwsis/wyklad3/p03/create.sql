
CREATE TABLE table_per_class_etatowy (
    idpracownika character varying(255) NOT NULL,
    email character varying(255),
    imie character varying(255),
    nazwisko character varying(255),
    odkiedy timestamp without time zone,
    wynagrodzeniebrutto double precision NOT NULL
);
ALTER TABLE public.table_per_class_etatowy OWNER TO postgres;

CREATE TABLE table_per_class_konsultant (
    idpracownika character varying(255) NOT NULL,
    email character varying(255),
    imie character varying(255),
    nazwisko character varying(255),
    odkiedy timestamp without time zone,
    stawkazadzien double precision NOT NULL
);
ALTER TABLE public.table_per_class_konsultant OWNER TO postgres;


ALTER TABLE ONLY table_per_class_etatowy
    ADD CONSTRAINT table_per_class_etatowy_pkey PRIMARY KEY (idpracownika);
ALTER TABLE ONLY table_per_class_konsultant
    ADD CONSTRAINT table_per_class_konsultant_pkey PRIMARY KEY (idpracownika);