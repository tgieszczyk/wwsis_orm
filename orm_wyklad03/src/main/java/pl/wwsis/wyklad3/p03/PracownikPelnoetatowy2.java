package pl.wwsis.wyklad3.p03;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TABLE_PER_CLASS_ETATOWY")
public class PracownikPelnoetatowy2 extends Pracownik2 {
	private double wynagrodzenieBrutto;

	public double getWynagrodzenieBrutto() {
		return wynagrodzenieBrutto;
	}

	public void setWynagrodzenieBrutto(double wynagrodzenieBrutto) {
		this.wynagrodzenieBrutto = wynagrodzenieBrutto;
	}

}
