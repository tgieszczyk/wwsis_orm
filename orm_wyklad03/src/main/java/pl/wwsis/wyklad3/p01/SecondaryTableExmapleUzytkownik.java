package pl.wwsis.wyklad3.p01;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;

@Entity
@Table(name = "UZYTKOWNIK_TABLE_02")
@SecondaryTables({ @SecondaryTable(name = "RELACJA_U_T_02"),
		@SecondaryTable(name = "RELACJA_U_T_03") })
public class SecondaryTableExmapleUzytkownik {
	@Id
	private long id;
	private String email;
	private String nip;

	@Column(name = "NAZWA", table = "RELACJA_U_T_03")
	private String nazwa;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

}
