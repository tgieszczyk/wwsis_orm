package pl.wwsis.wyklad3.p01;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "UZYTKOWNIK_TABLE_01", uniqueConstraints = { @UniqueConstraint(columnNames = {
		"email", "nip" }) }, indexes = {@Index(columnList  = "email")})
public class TableExmapleUzytkownik {
	@Id
	private long id;
	private String email;
	private String nip;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

}
