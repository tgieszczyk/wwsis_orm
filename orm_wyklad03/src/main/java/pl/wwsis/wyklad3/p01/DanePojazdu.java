package pl.wwsis.wyklad3.p01;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DANE_POJAZDU")
public class DanePojazdu {
//	@Id
//	private long id;

	@Id
	@Column(name = "pojazd_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long pojazdId;

//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}

	public long getPojazdId() {
		return pojazdId;
	}

	public void setPojazdId(long pojazdId) {
		this.pojazdId = pojazdId;
	}

}
