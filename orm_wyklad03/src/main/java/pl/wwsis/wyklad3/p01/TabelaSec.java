package pl.wwsis.wyklad3.p01;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RELACJA_U_T_031")
public class TabelaSec {

	@Id
	private long id;
	@Column(name = "NAZWA")
	private String nazwa;
}
