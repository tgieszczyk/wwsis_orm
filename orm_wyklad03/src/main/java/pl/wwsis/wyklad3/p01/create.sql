
CREATE TABLE dane_pojazdu (
    rok_produkcji integer,
    pojazd_id bigint NOT NULL
);


ALTER TABLE public.dane_pojazdu OWNER TO postgres;


CREATE TABLE pojazd (
    id bigint NOT NULL,
    marka character varying(255)
);


ALTER TABLE public.pojazd OWNER TO postgres;

CREATE TABLE relacja_u_t_02 (
    id bigint NOT NULL
);


ALTER TABLE public.relacja_u_t_02 OWNER TO postgres;

CREATE TABLE relacja_u_t_03 (
    nazwa character varying(255),
    id bigint NOT NULL
);


ALTER TABLE public.relacja_u_t_03 OWNER TO postgres;

CREATE TABLE relacja_u_t_031 (
    id bigint NOT NULL,
    nazwa character varying(255)
);


ALTER TABLE public.relacja_u_t_031 OWNER TO postgres;


CREATE TABLE uzytkownik_001 (
    id bigint NOT NULL,
    name character varying(255),
    nip character varying(255)
);


ALTER TABLE public.uzytkownik_001 OWNER TO postgres;


CREATE TABLE uzytkownik_002 (
    id bigint NOT NULL,
    name character varying(255),
    nip character varying(255)
);


ALTER TABLE public.uzytkownik_002 OWNER TO postgres;

CREATE TABLE uzytkownik_table_01 (
    id bigint NOT NULL,
    email character varying(255),
    nip character varying(255)
);


ALTER TABLE public.uzytkownik_table_01 OWNER TO postgres;

CREATE TABLE uzytkownik_table_02 (
    id bigint NOT NULL,
    email character varying(255),
    nip character varying(255)
);


ALTER TABLE public.uzytkownik_table_02 OWNER TO postgres;

ALTER TABLE ONLY dane_pojazdu
    ADD CONSTRAINT dane_pojazdu_pkey PRIMARY KEY (pojazd_id);

ALTER TABLE ONLY pojazd
    ADD CONSTRAINT pojazd_pkey PRIMARY KEY (id);

ALTER TABLE ONLY relacja_u_t_02
    ADD CONSTRAINT relacja_u_t_02_pkey PRIMARY KEY (id);


ALTER TABLE ONLY relacja_u_t_031
    ADD CONSTRAINT relacja_u_t_031_pkey PRIMARY KEY (id);


ALTER TABLE ONLY relacja_u_t_03
    ADD CONSTRAINT relacja_u_t_03_pkey PRIMARY KEY (id);

ALTER TABLE ONLY uzytkownik_table_01
    ADD CONSTRAINT uk_865kewpkmy2hhckmwoc5u4vnf UNIQUE (email, nip);

ALTER TABLE ONLY uzytkownik_001
    ADD CONSTRAINT uzytkownik_001_pkey PRIMARY KEY (id);

ALTER TABLE ONLY uzytkownik_002
    ADD CONSTRAINT uzytkownik_002_pkey PRIMARY KEY (id);

ALTER TABLE ONLY uzytkownik_table_01
    ADD CONSTRAINT uzytkownik_table_01_pkey PRIMARY KEY (id);

ALTER TABLE ONLY uzytkownik_table_02
    ADD CONSTRAINT uzytkownik_table_02_pkey PRIMARY KEY (id);

CREATE INDEX uk_481csqewexghqqpga7sc1subi ON uzytkownik_table_01 USING btree (email);



ALTER TABLE ONLY dane_pojazdu
    ADD CONSTRAINT fk_5lg6ygyycaqms0s14kv0o54d1 FOREIGN KEY (pojazd_id) REFERENCES pojazd(id);

ALTER TABLE ONLY relacja_u_t_02
    ADD CONSTRAINT fk_est6whbx87vc0q4gtkjvsmdw8 FOREIGN KEY (id) REFERENCES uzytkownik_table_02(id);

ALTER TABLE ONLY relacja_u_t_03
    ADD CONSTRAINT fk_jwhqqg72mnugib15tv2de2yb0 FOREIGN KEY (id) REFERENCES uzytkownik_table_02(id);