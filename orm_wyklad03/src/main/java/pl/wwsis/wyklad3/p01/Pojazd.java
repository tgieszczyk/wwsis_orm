package pl.wwsis.wyklad3.p01;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;

@Entity
@Table(name = "POJAZD")
@SecondaryTables({ @SecondaryTable(name = "DANE_POJAZDU", 
pkJoinColumns = { @PrimaryKeyJoinColumn(name = "pojazd_id", 
	referencedColumnName = "id") }) })
public class Pojazd {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "MARKA")
	private String marka;
	@Column(name = "ROK_PRODUKCJI", table = "DANE_POJAZDU")
	private Integer rokProdukcji;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public Integer getRokProdukcji() {
		return rokProdukcji;
	}

	public void setRokProdukcji(Integer rokProdukcji) {
		this.rokProdukcji = rokProdukcji;
	}

}
