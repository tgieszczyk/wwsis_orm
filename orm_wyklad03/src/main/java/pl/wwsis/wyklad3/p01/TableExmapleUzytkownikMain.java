package pl.wwsis.wyklad3.p01;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TableExmapleUzytkownikMain {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit-postgresql");

		EntityManager em = factory.createEntityManager();

			
			TableExmapleUzytkownik test = new TableExmapleUzytkownik();
			test.setEmail("tgieszczyk@gmail.com");
			test.setNip("9889889889");
			test.setId(1);
			save(em, test);
			
			SecondaryTableExmapleUzytkownik s = new SecondaryTableExmapleUzytkownik();
//			test = new TableExmapleUzytkownik();
			s.setEmail("trgieszczyk@gmail.com");
			s.setNip("9889889889");
			s.setId(2);
			s.setNazwa("jakas nazwa");
			save(em, s);
			
			s.setNazwa(null);
			update(em, s);
			
			s.setNazwa(" ");
			update(em, s);
			
		
		System.exit(0);
	}
	
	
	public static <T> void update(EntityManager em, T encja) {
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			em.merge(encja);
			
		} finally {
			transaction.commit();
		}
	}	
	public static <T> void save(EntityManager em, T encja) {
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			em.persist(encja);
			
		} finally {
			transaction.commit();
		}
	}

}
