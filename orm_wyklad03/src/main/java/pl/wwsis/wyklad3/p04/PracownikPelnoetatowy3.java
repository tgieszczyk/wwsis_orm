package pl.wwsis.wyklad3.p04;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("ETATOWY")
public class PracownikPelnoetatowy3 extends Pracownik3 {
	private double wynagrodzenieBrutto;

	public double getWynagrodzenieBrutto() {
		return wynagrodzenieBrutto;
	}

	public void setWynagrodzenieBrutto(double wynagrodzenieBrutto) {
		this.wynagrodzenieBrutto = wynagrodzenieBrutto;
	}

}
