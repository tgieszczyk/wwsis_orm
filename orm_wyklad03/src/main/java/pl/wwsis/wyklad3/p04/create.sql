CREATE TABLE single_table_pracownik
(
  rodzaj_pracownika character varying(31) NOT NULL,
  idpracownika character varying(255) NOT NULL,
  email character varying(255),
  imie character varying(255),
  nazwisko character varying(255),
  odkiedy timestamp without time zone,
  wynagrodzeniebrutto double precision,
  stawkazadzien double precision,
  CONSTRAINT single_table_pracownik_pkey PRIMARY KEY (idpracownika)
)