package pl.wwsis.wyklad3.p04;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@DiscriminatorValue("KONSULTANT")
public class Konsultant3 extends Pracownik3 {
	private double stawkaZaDzien;

	public double getStawkaZaDzien() {
		return stawkaZaDzien;
	}

	public void setStawkaZaDzien(double stawkaZaDzien) {
		this.stawkaZaDzien = stawkaZaDzien;
	}
	@Override
	public Float getWynagrodzenie() {
		
		return new Float(getStawkaZaDzien());
	}
}
