package pl.wwsis.wyklad3.p02;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "JOINED_PRACOWNIK")
public class Pracownik1 {
	@Id
	private String idPracownika;
	private String imie;
	private String nazwisko;
	private Date odKiedy;
	private String email;

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getIdPracownika() {
		return idPracownika;
	}

	public void setIdPracownika(String idPracownika) {
		this.idPracownika = idPracownika;
	}

	public Date getOdKiedy() {
		return odKiedy;
	}

	public void setOdKiedy(Date odKiedy) {
		this.odKiedy = odKiedy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
