package pl.wwsis.wyklad3.p02;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "JOINED_KONSULTANT")
public class Konsultant1 extends Pracownik1 {
	private double stawkaZaDzien;

	public double getStawkaZaDzien() {
		return stawkaZaDzien;
	}

	public void setStawkaZaDzien(double stawkaZaDzien) {
		this.stawkaZaDzien = stawkaZaDzien;
	}
}
