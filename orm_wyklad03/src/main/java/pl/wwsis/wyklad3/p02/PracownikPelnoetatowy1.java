package pl.wwsis.wyklad3.p02;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "JOINED_ETATOWY")
public class PracownikPelnoetatowy1 extends Pracownik1 {
	private double wynagrodzenieBrutto;

	public double getWynagrodzenieBrutto() {
		return wynagrodzenieBrutto;
	}

	public void setWynagrodzenieBrutto(double wynagrodzenieBrutto) {
		this.wynagrodzenieBrutto = wynagrodzenieBrutto;
	}

}
