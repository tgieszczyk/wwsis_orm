CREATE TABLE joined_etatowy (
    wynagrodzeniebrutto double precision NOT NULL,
    idpracownika character varying(255) NOT NULL
);
ALTER TABLE public.joined_etatowy OWNER TO postgres;

CREATE TABLE joined_konsultant (
    stawkazadzien double precision NOT NULL,
    idpracownika character varying(255) NOT NULL
);
ALTER TABLE public.joined_konsultant OWNER TO postgres;


CREATE TABLE joined_pracownik (
    idpracownika character varying(255) NOT NULL,
    email character varying(255),
    imie character varying(255),
    nazwisko character varying(255),
    odkiedy timestamp without time zone
);
ALTER TABLE public.joined_pracownik OWNER TO postgres;

ALTER TABLE ONLY joined_etatowy
    ADD CONSTRAINT joined_etatowy_pkey PRIMARY KEY (idpracownika);

ALTER TABLE ONLY joined_konsultant
    ADD CONSTRAINT joined_konsultant_pkey PRIMARY KEY (idpracownika);

ALTER TABLE ONLY joined_pracownik
    ADD CONSTRAINT joined_pracownik_pkey PRIMARY KEY (idpracownika);

ALTER TABLE ONLY joined_etatowy
    ADD CONSTRAINT fk_3i3grl4lmjsvrmxgmxemc9l11 FOREIGN KEY (idpracownika) REFERENCES joined_pracownik(idpracownika);

ALTER TABLE ONLY joined_konsultant
    ADD CONSTRAINT fk_6t1hs2051bi0fmfwordkat4fy FOREIGN KEY (idpracownika) REFERENCES joined_pracownik(idpracownika);

