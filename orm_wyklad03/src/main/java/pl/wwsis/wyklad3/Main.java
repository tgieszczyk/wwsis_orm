package pl.wwsis.wyklad3;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.wyklad3.p01.Pojazd;
import pl.wwsis.wyklad3.p04.Konsultant3;
import pl.wwsis.wyklad3.p04.Pracownik3;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit-postgresql");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();
		Pojazd p = new Pojazd();
		p.setMarka("Tesla S");
		p.setRokProdukcji(2015);
//		p.setId(1l);
		

		Konsultant3 pr = new Konsultant3();
		pr.setImie("Michał");
		pr.setNazwisko("Kowalski");
		pr.setIdPracownika("A009898");
		pr.setEmail("m.kowaslik@gmail.com");
		pr.setOdKiedy(new Date());
		pr.setStawkaZaDzien(10000d);
		
		try {
			transaction.begin();
			em.persist(pr);

		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
		System.exit(0);
	}

}
