package pl.wwsis.esklep.encje;

/**
 * @author ORM
 *
 */
public enum RodzajZdjecia {
	JPG, GIF, PNG;
}
