package pl.wwsis.esklep.encje.listeners;

import java.util.Date;

import javax.annotation.PreDestroy;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import pl.wwsis.esklep.encje.StatusEncji;
import pl.wwsis.esklep.encje.Towar;

public class TowarListener {

//	@PrePersist
//	@PreUpdate
//	public void uaktualnijDateOstatniejAktualizacji(Towar towar) {
//		//
//		towar.setDataDodania(new Date());
//		towar.setDodanePrzez("Tomasz Gieszczyk");
//	}
//
//	@PreUpdate
//	public void logUpdate(Towar towar) {
//		
//	}
//	@PrePersist
//	public void logPersist(Towar towar) {
//		
//	}

	@PostPersist
	public void uaktualnijStatusEncji(Towar towar) {
		towar.setStatusEncji(StatusEncji.ZAPISANA);
	}
	
	
//	@PostLoad
//	@PostPersist
//	@PostRemove
//	@PostUpdate
//	
//	@PrePersist
//	@PreUpdate
//	@PreDestroy
//	@PreRemove
//	public void cosTab(Towar t) {
//		
//	}
}
