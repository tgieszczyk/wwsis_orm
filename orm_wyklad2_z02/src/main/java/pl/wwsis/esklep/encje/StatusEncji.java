package pl.wwsis.esklep.encje;

/**
 * @author ORM
 *
 */
public enum StatusEncji {
	ODCZYTANA, ZAPISANA;
}
