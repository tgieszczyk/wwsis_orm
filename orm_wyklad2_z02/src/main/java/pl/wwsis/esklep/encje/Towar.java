package pl.wwsis.esklep.encje;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ExcludeDefaultListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import pl.wwsis.esklep.encje.listeners.TowarListener;

/**
 * Zamiana na duze litery Ctrl+Shift+X Zamiana na male litery Ctrl+Shift+Y
 * 
 * @author ORM
 *
 */
@Entity
@Table(name = "TOWAR")
@SecondaryTable(name = "DETALE_TOWARU", pkJoinColumns = {@PrimaryKeyJoinColumn(referencedColumnName = "ID", name = "ID_TOWARU")} )
@EntityListeners({ TowarListener.class })
@ExcludeDefaultListeners
public class Towar implements Serializable {

	@Id
	// Aby moc operowac na sekwencjach, nalezy stworozyc ja najpierw w bazie
	// danych, tutaj tworzymy sekwencje o nazwwie SEQ_TOWAR
	// nastepnie mapujemy sekwencje jako generator klucza glownego dla encji
	// Towar w nastepujacy sposob
	@SequenceGenerator(name = "generator_sew_towar", sequenceName = "SEQ_TOWAR", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_sew_towar")
	private long id;
	private String nazwa;
	private double cena;

	@Column(name = "RODZAJ_TOWARU")
	@Enumerated(EnumType.STRING)
	private RodzajTowaru rodzajTowaru;

	// W bazie danych pole dataDodania istnieje w tabeli Towar w kolumnie o
	// nazwie DATA_DODANIA
	@Column(name = "DATA_DODANIA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDodania;

	@Column(name = "DODANA_PRZEZ")
	private String dodanePrzez;

	@Column(name = "DATA_WYSTAWIENIA_OD")
	@Temporal(TemporalType.DATE)
	private Date dataWystawieniaOd;

	@Column(name = "DATA_WYSTAWIENIA_DO")
	@Temporal(TemporalType.DATE)
	private Date dataWystawieniaDo;

	@Column(table = "DETALE_TOWARU")
	@Lob
	// fetchType.Lazy oznacza, ze pole nie jest mapowane od razu podczas
	// tworzenia encji, ale w momencie kiedy chcemy je pobrac z obiektu java
	@Basic(fetch = FetchType.LAZY)
	private byte[] zdjecie;

	@Column(table = "DETALE_TOWARU", name = "OPIS_TOWARU")
	private String opisTowaru;

	@Enumerated(EnumType.STRING)
	@Column(table = "DETALE_TOWARU", name = "TYP_ZDJECIA")
	private RodzajZdjecia rodzajZdjecia;

	@Transient
	private StatusEncji statusEncji = StatusEncji.ODCZYTANA;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public RodzajTowaru getRodzajTowaru() {
		return rodzajTowaru;
	}

	public void setRodzajTowaru(RodzajTowaru rodzajTowaru) {
		this.rodzajTowaru = rodzajTowaru;
	}

	public Date getDataDodania() {
		return dataDodania;
	}

	public void setDataDodania(Date dataDodania) {
		this.dataDodania = dataDodania;
	}

	public String getDodanePrzez() {
		return dodanePrzez;
	}

	public void setDodanePrzez(String dodanePrzez) {
		this.dodanePrzez = dodanePrzez;
	}

	public Date getDataWystawieniaOd() {
		return dataWystawieniaOd;
	}

	public void setDataWystawieniaOd(Date dataWystawieniaOd) {
		this.dataWystawieniaOd = dataWystawieniaOd;
	}

	public Date getDataWystawieniaDo() {
		return dataWystawieniaDo;
	}

	public void setDataWystawieniaDo(Date dataWystawieniaDo) {
		this.dataWystawieniaDo = dataWystawieniaDo;
	}

	public byte[] getZdjecie() {
		return zdjecie;
	}

	public void setZdjecie(byte[] zdjecie) {
		this.zdjecie = zdjecie;
	}

	public String getOpisTowaru() {
		return opisTowaru;
	}

	public void setOpisTowaru(String opisTowaru) {
		this.opisTowaru = opisTowaru;
	}

	public RodzajZdjecia getRodzajZdjecia() {
		return rodzajZdjecia;
	}

	public void setRodzajZdjecia(RodzajZdjecia rodzajZdjecia) {
		this.rodzajZdjecia = rodzajZdjecia;
	}

	public StatusEncji getStatusEncji() {
		return statusEncji;
	}

	public void setStatusEncji(StatusEncji statusEncji) {
		this.statusEncji = statusEncji;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(cena);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((dataDodania == null) ? 0 : dataDodania.hashCode());
		result = prime * result + ((dataWystawieniaDo == null) ? 0 : dataWystawieniaDo.hashCode());
		result = prime * result + ((dataWystawieniaOd == null) ? 0 : dataWystawieniaOd.hashCode());
		result = prime * result + ((dodanePrzez == null) ? 0 : dodanePrzez.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		result = prime * result + ((opisTowaru == null) ? 0 : opisTowaru.hashCode());
		result = prime * result + ((rodzajTowaru == null) ? 0 : rodzajTowaru.hashCode());
		result = prime * result + ((rodzajZdjecia == null) ? 0 : rodzajZdjecia.hashCode());
		result = prime * result + Arrays.hashCode(zdjecie);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Towar other = (Towar) obj;
		if (Double.doubleToLongBits(cena) != Double.doubleToLongBits(other.cena))
			return false;
		if (dataDodania == null) {
			if (other.dataDodania != null)
				return false;
		} else if (!dataDodania.equals(other.dataDodania))
			return false;
		if (dataWystawieniaDo == null) {
			if (other.dataWystawieniaDo != null)
				return false;
		} else if (!dataWystawieniaDo.equals(other.dataWystawieniaDo))
			return false;
		if (dataWystawieniaOd == null) {
			if (other.dataWystawieniaOd != null)
				return false;
		} else if (!dataWystawieniaOd.equals(other.dataWystawieniaOd))
			return false;
		if (dodanePrzez == null) {
			if (other.dodanePrzez != null)
				return false;
		} else if (!dodanePrzez.equals(other.dodanePrzez))
			return false;
		if (id != other.id)
			return false;
		if (nazwa == null) {
			if (other.nazwa != null)
				return false;
		} else if (!nazwa.equals(other.nazwa))
			return false;
		if (opisTowaru == null) {
			if (other.opisTowaru != null)
				return false;
		} else if (!opisTowaru.equals(other.opisTowaru))
			return false;
		if (rodzajTowaru != other.rodzajTowaru)
			return false;
		if (rodzajZdjecia != other.rodzajZdjecia)
			return false;
		if (!Arrays.equals(zdjecie, other.zdjecie))
			return false;
		return true;
	}

}
