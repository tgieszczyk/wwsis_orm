package pl.wwsis;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.esklep.encje.RodzajTowaru;
import pl.wwsis.esklep.encje.RodzajZdjecia;
import pl.wwsis.esklep.encje.Towar;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		// jesteśmy gotowi aby tworzyć encję

		EntityTransaction transaction = em.getTransaction();

		try {
			Towar towar1 = new Towar();
			Towar towar2 = new Towar();
			
			// nie wypelnilem pol z secondaryTable!!!
			towar1.setNazwa("Ala");
			towar1.setCena(200D);
			towar1.setRodzajTowaru(RodzajTowaru.ROZRYWKA);
			
			// wypelnilem pola z secondaryTable
			towar2.setNazwa("Krzeslo");
			towar2.setOpisTowaru("Zielone, czteronozne");
			towar2.setCena(120D);
			towar2.setRodzajTowaru(RodzajTowaru.ROZRYWKA);
			towar2.setRodzajZdjecia(RodzajZdjecia.JPG);

			System.out.println(towar1.getStatusEncji());
			
			transaction.begin();
			
			em.persist(towar1);
			em.persist(towar2);

			// wykonujemy operacje create, delete, update
			//em.persist(p);
			// em.remove
			// em.merge

			transaction.commit();
			
			towar1.setOpisTowaru("jakis uzupelniajacy opis");
			
			transaction.begin();
			em.merge(towar1);
			transaction.commit();

			System.out.println(towar1.getStatusEncji());
			System.exit(0);
		} catch (Exception e) {

			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}

}
