package pl.wwsis;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.pojazdy.bulders.DanePojazduBuilder;
import pl.wwsis.pojazdy.encje.DanePojazdu;
import pl.wwsis.pojazdy.encje.Pojazd;
import pl.wwsis.pojazdy.encje.TypNadwozia;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();

		try {

			DanePojazdu danePojazdu = new DanePojazdu().withDataProdukcji(new Date()).withLiczbaDrzwi(2)
					.withLiczbaMiejsc(2).withMarka("Toyota GT86").withPojemnoscSilnika(1998D)
					.withTypNadwozia(TypNadwozia.COUPE);
			// new DanePojazdu();
			// danePojazdu.setDataProdukcji(new Date());
			// danePojazdu.setLiczbaDrzwi(3);
			// danePojazdu.setLiczbaMiejsc(2);
			// danePojazdu.setMarka("Toyota GT86");
			// danePojazdu.setPojemnoscSilnika(1998d);
			// danePojazdu.setTypNadwozia(TypNadwozia.COUPE);

			Pojazd pojazd = new Pojazd();
			pojazd.setCena(130000D);
			pojazd.setDataRejestracji(new Date());
			pojazd.setDanePojazdu(danePojazdu);

			transaction.begin();

			em.persist(pojazd);

			// wykonujemy operacje create, delete, update
			// em.persist(p);
			// em.remove
			// em.merge

			transaction.commit();
			System.exit(0);
		} catch (Exception e) {

			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}

}
