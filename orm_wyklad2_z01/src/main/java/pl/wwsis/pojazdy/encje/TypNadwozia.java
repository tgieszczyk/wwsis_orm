package pl.wwsis.pojazdy.encje;

/**
 * @author ORM
 *
 */
public enum TypNadwozia {
	SEDAN, CABRIO, KOMBII, COMPACT, COUPE;
}
