package pl.wwsis.pojazdy.encje;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ORM
 *
 */
@Entity
public class Pojazd implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	// dane z obiektu DanePojazdu oraz z encji Pojazd beda zapisane w tej samej
	// tabeli w bazie danych
	@Embedded
	private DanePojazdu danePojazdu;
	private Double cena;
	@Temporal(TemporalType.DATE)
	private Date dataRejestracji;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public DanePojazdu getDanePojazdu() {
		return danePojazdu;
	}

	public void setDanePojazdu(DanePojazdu danePojazdu) {
		this.danePojazdu = danePojazdu;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Date getDataRejestracji() {
		return dataRejestracji;
	}

	public void setDataRejestracji(Date dataRejestracji) {
		this.dataRejestracji = dataRejestracji;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cena == null) ? 0 : cena.hashCode());
		result = prime * result + ((danePojazdu == null) ? 0 : danePojazdu.hashCode());
		result = prime * result + ((dataRejestracji == null) ? 0 : dataRejestracji.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pojazd other = (Pojazd) obj;
		if (cena == null) {
			if (other.cena != null)
				return false;
		} else if (!cena.equals(other.cena))
			return false;
		if (danePojazdu == null) {
			if (other.danePojazdu != null)
				return false;
		} else if (!danePojazdu.equals(other.danePojazdu))
			return false;
		if (dataRejestracji == null) {
			if (other.dataRejestracji != null)
				return false;
		} else if (!dataRejestracji.equals(other.dataRejestracji))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

}
