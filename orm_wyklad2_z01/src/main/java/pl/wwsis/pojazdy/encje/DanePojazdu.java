package pl.wwsis.pojazdy.encje;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ORM
 *
 */
// aby dodac DatePojazdu do encji Pojazd jako typ wlasny nalezy oznaczyc ja
// encja Embeddable
// pozwala to na dodanie tych danych do tej samej tabeli co danych z encji
// Pojazd
@Embeddable
public class DanePojazdu implements Serializable {
	private String marka;
	@Temporal(TemporalType.DATE)
	private Date dataProdukcji;
	private double pojemnoscSilnika;
	private int liczbaDrzwi;
	@Enumerated(EnumType.STRING)
	private TypNadwozia typNadwozia;
	private int liczbaMiejsc;

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public Date getDataProdukcji() {
		return dataProdukcji;
	}

	public void setDataProdukcji(Date dataProdukcji) {
		this.dataProdukcji = dataProdukcji;
	}

	public double getPojemnoscSilnika() {
		return pojemnoscSilnika;
	}

	public void setPojemnoscSilnika(double pojemnoscSilnika) {
		this.pojemnoscSilnika = pojemnoscSilnika;
	}

	public int getLiczbaDrzwi() {
		return liczbaDrzwi;
	}

	public void setLiczbaDrzwi(int liczbaDrzwi) {
		this.liczbaDrzwi = liczbaDrzwi;
	}

	public TypNadwozia getTypNadwozia() {
		return typNadwozia;
	}

	public void setTypNadwozia(TypNadwozia typNadwozia) {
		this.typNadwozia = typNadwozia;
	}

	public int getLiczbaMiejsc() {
		return liczbaMiejsc;
	}

	public void setLiczbaMiejsc(int liczbaMiejsc) {
		this.liczbaMiejsc = liczbaMiejsc;
	}

	public DanePojazdu withMarka(String marka) {
		setMarka(marka);
		return this;
	}

	public DanePojazdu withDataProdukcji(Date dataProdukcji) {
		setDataProdukcji(dataProdukcji);
		return this;
	}

	public DanePojazdu withPojemnoscSilnika(double pojemnoscSilnika) {
		setPojemnoscSilnika(pojemnoscSilnika);
		return this;
	}

	public DanePojazdu withLiczbaDrzwi(int liczbaDrzwi) {
		setLiczbaDrzwi(liczbaDrzwi);
		return this;
	}

	public DanePojazdu withTypNadwozia(TypNadwozia typNadwozia) {
		setTypNadwozia(typNadwozia);
		return this;
	}

	public DanePojazdu withLiczbaMiejsc(int liczbaMiejsc) {
		setLiczbaMiejsc(liczbaMiejsc);
		return this;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataProdukcji == null) ? 0 : dataProdukcji.hashCode());
		result = prime * result + liczbaDrzwi;
		result = prime * result + liczbaMiejsc;
		result = prime * result + ((marka == null) ? 0 : marka.hashCode());
		long temp;
		temp = Double.doubleToLongBits(pojemnoscSilnika);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((typNadwozia == null) ? 0 : typNadwozia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DanePojazdu other = (DanePojazdu) obj;
		if (dataProdukcji == null) {
			if (other.dataProdukcji != null)
				return false;
		} else if (!dataProdukcji.equals(other.dataProdukcji))
			return false;
		if (liczbaDrzwi != other.liczbaDrzwi)
			return false;
		if (liczbaMiejsc != other.liczbaMiejsc)
			return false;
		if (marka == null) {
			if (other.marka != null)
				return false;
		} else if (!marka.equals(other.marka))
			return false;
		if (Double.doubleToLongBits(pojemnoscSilnika) != Double.doubleToLongBits(other.pojemnoscSilnika))
			return false;
		if (typNadwozia != other.typNadwozia)
			return false;
		return true;
	}

}
