package pl.wwsis.pojazdy.bulders;

import java.util.Date;

import pl.wwsis.pojazdy.encje.DanePojazdu;
import pl.wwsis.pojazdy.encje.TypNadwozia;

public class DanePojazduBuilder {
	private DanePojazdu danePojazdu;

	public DanePojazduBuilder withMarka(String marka) {
		danePojazdu.setMarka(marka);
		return this;
	}

	public DanePojazduBuilder withDataProdukcji(Date dataProdukcji) {
		danePojazdu.setDataProdukcji(dataProdukcji);
		return this;
	}

	public DanePojazduBuilder withPojemnoscSilnika(double pojemnoscSilnika) {
		danePojazdu.setPojemnoscSilnika(pojemnoscSilnika);
		return this;
	}

	public DanePojazduBuilder withLiczbaDrzwi(int liczbaDrzwi) {
		danePojazdu.setLiczbaDrzwi(liczbaDrzwi);
		return this;
	}

	public DanePojazduBuilder withTypNadwozia(TypNadwozia typNadwozia) {
		danePojazdu.setTypNadwozia(typNadwozia);
		return this;
	}

	public DanePojazduBuilder withLiczbaMiejsc(int liczbaMiejsc) {
		danePojazdu.setLiczbaMiejsc(liczbaMiejsc);
		return this;
	}

	public DanePojazdu getDanePojazdu() {
		return danePojazdu;
	}

}
