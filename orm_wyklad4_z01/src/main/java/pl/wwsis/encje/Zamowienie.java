package pl.wwsis.encje;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author ORM
 *
 */
@Entity
@Table(name = "ZAMOWIENIA")
public class Zamowienie {

	@Id
	@SequenceGenerator(name = "GEN_SEQ_WSPOLNA", sequenceName = "SEQ_WSPOLNA", allocationSize= 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_SEQ_WSPOLNA")
	private long id;

	@Column(name = "NUMER_EWIDENCYJNY")
	private String numerEwidencyjny;

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH})
	@JoinColumn(name = "id_zamowienia")
	private List<PozycjaZamowienia> pozycjeZamowienia;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumerEwidencyjny() {
		return numerEwidencyjny;
	}

	public void setNumerEwidencyjny(String numerEwidencyjny) {
		this.numerEwidencyjny = numerEwidencyjny;
	}

	public List<PozycjaZamowienia> getPozycjeZamowienia() {
		if (pozycjeZamowienia == null) {
			pozycjeZamowienia = new ArrayList<>();
		}
		return pozycjeZamowienia;
	}

	public void setPozycjeZamowienia(List<PozycjaZamowienia> pozycjeZamowienia) {
		this.pozycjeZamowienia = pozycjeZamowienia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((numerEwidencyjny == null) ? 0 : numerEwidencyjny.hashCode());
		result = prime * result + ((pozycjeZamowienia == null) ? 0 : pozycjeZamowienia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zamowienie other = (Zamowienie) obj;
		if (id != other.id)
			return false;
		if (numerEwidencyjny == null) {
			if (other.numerEwidencyjny != null)
				return false;
		} else if (!numerEwidencyjny.equals(other.numerEwidencyjny))
			return false;
		if (pozycjeZamowienia == null) {
			if (other.pozycjeZamowienia != null)
				return false;
		} else if (!pozycjeZamowienia.equals(other.pozycjeZamowienia))
			return false;
		return true;
	}

}
