package pl.wwsis.encje;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author ORM
 *
 */
@Entity
@Table(name = "POZYCJA_ZAMOWIENIA")
public class PozycjaZamowienia {

	@Id
	@SequenceGenerator(name = "GEN_SEQ_WSPOLNA", sequenceName = "SEQ_WSPOLNA", allocationSize= 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_SEQ_WSPOLNA")
	private long id;

	private String nazwa;
	@Column(name = "KWOTA_NETTO")
	private double kwotaNetto;
	@Column(name = "KWOTA_BRUTTO")
	private double kwotaBrutto;
	@Column(name = "STAWKA_VAT")
	private double stawkaVat;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public double getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(double kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public double getKwotaBrutto() {
		return kwotaBrutto;
	}

	public void setKwotaBrutto(double kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}

	public double getStawkaVat() {
		return stawkaVat;
	}

	public void setStawkaVat(double stawkaVat) {
		this.stawkaVat = stawkaVat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		long temp;
		temp = Double.doubleToLongBits(kwotaBrutto);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(kwotaNetto);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		temp = Double.doubleToLongBits(stawkaVat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PozycjaZamowienia other = (PozycjaZamowienia) obj;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(kwotaBrutto) != Double.doubleToLongBits(other.kwotaBrutto))
			return false;
		if (Double.doubleToLongBits(kwotaNetto) != Double.doubleToLongBits(other.kwotaNetto))
			return false;
		if (nazwa == null) {
			if (other.nazwa != null)
				return false;
		} else if (!nazwa.equals(other.nazwa))
			return false;
		if (Double.doubleToLongBits(stawkaVat) != Double.doubleToLongBits(other.stawkaVat))
			return false;
		return true;
	}

}
