package pl.wwsis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.encje.Klient;
import pl.wwsis.encje.PozycjaZamowienia;
import pl.wwsis.encje.Zamowienie;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();

		try {

			List<Klient> klienci = createKlienci();
			transaction.begin();
			for (Klient k : klienci) {

				// w mapowaniu dodaj cascade type Persist i merge aby moc
				// dodawac encje Klient bez potrzeby dodawania poszczegolnych
				// encji w relacji.
				
				// for(Zamowienie z : k.getZamowienia()) {
				// for(PozycjaZamowienia pz : z.getPozycjeZamowienia()) {
				// em.persist(pz);
				// em.flush();
				// }
				// em.persist(z);
				// em.flush();
				// }

				em.persist(k);
				em.flush();
			}

			// wykonujemy operacje create, delete, update
			// em.persist(p);
			// em.remove
			// em.merge

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
		System.exit(0);
	}

	private static List<PozycjaZamowienia> createPozycjeZamowienia(int index) {
		List<PozycjaZamowienia> result = new ArrayList<>();
		for (int i = index * 5; i < (index * 5) + 5; i++) {
			double kwotaBrutto = (i + 1) * 10;
			double kwotaNetto = kwotaBrutto / 1.23d;
			double stawkaVat = 23d;
			PozycjaZamowienia pz = new PozycjaZamowienia();
			pz.setKwotaBrutto(kwotaBrutto);
			pz.setKwotaNetto(kwotaNetto);
			pz.setStawkaVat(stawkaVat);
			pz.setNazwa("Buty(" + (i + 1) + ")");
			result.add(pz);
		}
		return result;
	}

	private static List<Zamowienie> createZamowienia(int index) {
		List<Zamowienie> result = new ArrayList<>();
		for (int i = index * 5; i < (index * 5) + 5; i++) {
			Zamowienie z = new Zamowienie();
			z.setNumerEwidencyjny("2016/" + (i + 1) * new Random().nextInt());
			z.getPozycjeZamowienia().addAll(createPozycjeZamowienia(i));
			result.add(z);
		}
		return result;
	}

	private static List<Klient> createKlienci() {
		List<Klient> result = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			Klient k = new Klient();
			k.setEmail(i + "@gmail.com");
			k.setKodPocztowy("54-43" + i);
			k.setKraj("Polska");
			k.setMiasto("Wroclaw");
			k.setNip("990-009-90-7" + i);
			k.setTelefon("+48 889-987-76" + i);
			k.setUlica("ul. Wejherowska " + i);
			k.setWww("---- brak -----");
			k.getZamowienia().addAll(createZamowienia(i));
			result.add(k);
		}
		return result;
	}

}
