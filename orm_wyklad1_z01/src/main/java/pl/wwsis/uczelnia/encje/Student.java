package pl.wwsis.uczelnia.encje;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author ORM
 *
 */
@Entity
public class Student {

	// przyjale ze numer indeksu bedzie wprowadzac Pani z dziekanatu
	@Id
	private String numerIndeksu;
	@Temporal(TemporalType.DATE)
	private Date studiujeOd;
	private String imie;
	private String nazwisko;
	private String adresZamieszkania;

	// Alt+Shift+S -> R
	public String getNumerIndeksu() {
		return numerIndeksu;
	}

	public void setNumerIndeksu(String numerIndeksu) {
		this.numerIndeksu = numerIndeksu;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getAdresZamieszkania() {
		return adresZamieszkania;
	}

	public void setAdresZamieszkania(String adresZamieszkania) {
		this.adresZamieszkania = adresZamieszkania;
	}

	public Date getStudiujeOd() {
		return studiujeOd;
	}

	public void setStudiujeOd(Date studiujeOd) {
		this.studiujeOd = studiujeOd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresZamieszkania == null) ? 0 : adresZamieszkania.hashCode());
		result = prime * result + ((imie == null) ? 0 : imie.hashCode());
		result = prime * result + ((nazwisko == null) ? 0 : nazwisko.hashCode());
		result = prime * result + ((numerIndeksu == null) ? 0 : numerIndeksu.hashCode());
		result = prime * result + ((studiujeOd == null) ? 0 : studiujeOd.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (adresZamieszkania == null) {
			if (other.adresZamieszkania != null)
				return false;
		} else if (!adresZamieszkania.equals(other.adresZamieszkania))
			return false;
		if (imie == null) {
			if (other.imie != null)
				return false;
		} else if (!imie.equals(other.imie))
			return false;
		if (nazwisko == null) {
			if (other.nazwisko != null)
				return false;
		} else if (!nazwisko.equals(other.nazwisko))
			return false;
		if (numerIndeksu == null) {
			if (other.numerIndeksu != null)
				return false;
		} else if (!numerIndeksu.equals(other.numerIndeksu))
			return false;
		if (studiujeOd == null) {
			if (other.studiujeOd != null)
				return false;
		} else if (!studiujeOd.equals(other.studiujeOd))
			return false;
		return true;
	}

	// Alt+Shift+S -> Generate hashcode and equals

	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
