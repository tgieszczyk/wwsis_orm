package pl.wwsis.uczelnia.encje;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author ORM
 *
 */
@Entity
public class Zajecia {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String numerIndeksuStudenta;
	private String nazwaZajec;
	private int liczbaGodzinWSemestrze;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumerIndeksuStudenta() {
		return numerIndeksuStudenta;
	}

	public void setNumerIndeksuStudenta(String numerIndeksuStudenta) {
		this.numerIndeksuStudenta = numerIndeksuStudenta;
	}

	public String getNazwaZajec() {
		return nazwaZajec;
	}

	public void setNazwaZajec(String nazwaZajec) {
		this.nazwaZajec = nazwaZajec;
	}

	public int getLiczbaGodzinWSemestrze() {
		return liczbaGodzinWSemestrze;
	}

	public void setLiczbaGodzinWSemestrze(int liczbaGodzinWSemestrze) {
		this.liczbaGodzinWSemestrze = liczbaGodzinWSemestrze;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + liczbaGodzinWSemestrze;
		result = prime * result + ((nazwaZajec == null) ? 0 : nazwaZajec.hashCode());
		result = prime * result + ((numerIndeksuStudenta == null) ? 0 : numerIndeksuStudenta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zajecia other = (Zajecia) obj;
		if (id != other.id)
			return false;
		if (liczbaGodzinWSemestrze != other.liczbaGodzinWSemestrze)
			return false;
		if (nazwaZajec == null) {
			if (other.nazwaZajec != null)
				return false;
		} else if (!nazwaZajec.equals(other.nazwaZajec))
			return false;
		if (numerIndeksuStudenta == null) {
			if (other.numerIndeksuStudenta != null)
				return false;
		} else if (!numerIndeksuStudenta.equals(other.numerIndeksuStudenta))
			return false;
		return true;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
