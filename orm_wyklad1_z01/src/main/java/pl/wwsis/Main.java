package pl.wwsis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.uczelnia.encje.Student;
import pl.wwsis.uczelnia.encje.Zajecia;

public class Main {

	public static void main(String[] args) throws ParseException {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		// wczytujemy nasze encje
		List<Student> listaStudentow = wczytajStudentow();
		List<Zajecia> listaZajec = wczytajZajecia();
		

		EntityTransaction transaction = em.getTransaction();

		try {

			transaction.begin();

			for (Student s : listaStudentow) {
				em.persist(s);
			}

			for (Zajecia z : listaZajec) {
				em.persist(z);
			}

			// wykonujemy operacje create, delete, update
			// em.persist(p);
			// em.remove
			// em.merge

			transaction.commit();
		} catch (Exception e) {

			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}

	private static SimpleDateFormat formatDaty = new SimpleDateFormat("yyyy-MM-dd");

	private static List<Student> wczytajStudentow() throws ParseException {
		String nazwaPliku = "/daneInicjujace-student.txt";
		Scanner scanner = new Scanner(Main.class.getResourceAsStream(nazwaPliku));
		List<Student> result = new ArrayList<Student>();
		try {
			boolean columnRowReaded = false;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (!columnRowReaded) {
					columnRowReaded = true;
					continue;
				}

				String[] cells = line.split("\t");
				Student student = new Student();
				student.setNumerIndeksu(cells[0]);
				student.setImie(cells[1]);
				student.setNazwisko(cells[2]);
				student.setAdresZamieszkania(cells[3]);
				student.setStudiujeOd(formatDaty.parse(cells[4]));

				result.add(student);
			}
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
		return result;
	}

	private static List<Zajecia> wczytajZajecia() {
		String nazwaPliku = "/daneInicjujace-zajecia.txt";
		Scanner scanner = new Scanner(Main.class.getResourceAsStream(nazwaPliku));
		List<Zajecia> result = new ArrayList<Zajecia>();
		try {
			boolean columnRowReaded = false;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (!columnRowReaded) {
					columnRowReaded = true;
					continue;
				}

				String[] cells = line.split("\t");
				Zajecia zajecia = new Zajecia();
				zajecia.setNumerIndeksuStudenta(cells[0]);
				zajecia.setNazwaZajec(cells[1]);
				zajecia.setLiczbaGodzinWSemestrze(Integer.parseInt(cells[2]));

				result.add(zajecia);
			}
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
		return result;
	}
}
