package pl.wwsis;

import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import pl.wwsis.uczelnia.encje.Student;

public class MainConsole {
	public static void main(String[] args) {
		wyswietlMenu();

		Scanner scanner = new Scanner(System.in);
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();
		
		String next;
		while ((next = scanner.nextLine()) != null) {
			try {
				Integer opcja = Integer.parseInt(next);
				switch (opcja) {
				case 1:
					obslugaWyszukiwaniaStudentow(scanner, em);
					break;
				case 2:
					obslugaDodawaniaZajec(scanner, em);
					break;
				case 3:
					obslugaDodawaniaStudentow(scanner, em);
					break;
				case 4:
					System.exit(0);
				}
			} catch (NumberFormatException e) {
				wyswietlMenu();
				System.out.println("Podaj prawidlowa opcje");
			}
			wyswietlMenu();
		}
	}

	private static void wyswietlMenu() {
		System.out.println("1 -> Wyszukaj studentow po numerze indeksu");
		System.out.println("2 -> Dodaj nowe zajecia");
		System.out.println("3 -> Dodaj nowego studenta");
		System.out.println("4 -> Wyjscie");
	}

	private static void obslugaWyszukiwaniaStudentow(Scanner scanner, EntityManager em) {
		System.out.print("Podaj index Studenta: ");
		String indexStudenta = scanner.next();
		System.out.println();
		if(indexStudenta != null) {
			try {
				Student student = em.find(Student.class, indexStudenta);
				
				System.out.println(student);
				System.out.print("Wcisnij dowolny przycisk aby kontynuowac");
				scanner.next();
			} catch (NoResultException e) {
				System.out.println("Student o podanym indeksie nie istnieje");
			}
		}
	}

	private static void obslugaDodawaniaStudentow(Scanner scanner, EntityManager em) {
		// TODO
	}

	private static void obslugaDodawaniaZajec(Scanner scanner, EntityManager em) {
		// TODO
	}
}
