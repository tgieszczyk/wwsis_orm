package pl.wwsis.encje;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.CascadeType.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author ORM
 *
 */
@Entity
@Table(name = "WYDAWNICTWO")
public class Wydawnictwo {

	@Id
	@SequenceGenerator(name = "GEN_SEQ_WSPOLNA", sequenceName = "SEQ_WSPOLNA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_SEQ_WSPOLNA")
	private long id;
	private String nazwa;

	@OneToOne(cascade = { PERSIST, REFRESH, MERGE, DETACH, REMOVE })
	@JoinColumn(name = "ADRES_SIEDZIBA_ID", referencedColumnName = "ID")
	private Adres adres;

	@ManyToMany(cascade = { PERSIST, REFRESH, MERGE, DETACH })
	@JoinTable(name = "WYDAWNICTWO_AUTOR", inverseJoinColumns = {
			@JoinColumn(name = "AUTOR_ID", referencedColumnName = "ID") }, joinColumns = {
					@JoinColumn(referencedColumnName = "ID", name = "WYDAWNICTWO_ID") })
	private List<Autor> autorzy;

	@OneToMany(mappedBy = "wydawnictwo", cascade = { REFRESH, DETACH, REMOVE })
	private List<Ksiazka> ksiazki;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Adres getAdres() {
		return adres;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	public List<Autor> getAutorzy() {
		if (autorzy == null) {
			autorzy = new ArrayList<>();
		}
		return autorzy;
	}

	public void setAutorzy(List<Autor> autorzy) {
		this.autorzy = autorzy;
	}

	public List<Ksiazka> getKsiazki() {
		if (ksiazki == null) {
			ksiazki = new ArrayList<>();
		}
		return ksiazki;
	}

	public void setKsiazki(List<Ksiazka> ksiazki) {
		this.ksiazki = ksiazki;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adres == null) ? 0 : adres.hashCode());
		result = prime * result + ((autorzy == null) ? 0 : autorzy.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((ksiazki == null) ? 0 : ksiazki.hashCode());
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Wydawnictwo other = (Wydawnictwo) obj;
		if (adres == null) {
			if (other.adres != null)
				return false;
		} else if (!adres.equals(other.adres))
			return false;
		if (autorzy == null) {
			if (other.autorzy != null)
				return false;
		} else if (!autorzy.equals(other.autorzy))
			return false;
		if (id != other.id)
			return false;
		if (ksiazki == null) {
			if (other.ksiazki != null)
				return false;
		} else if (!ksiazki.equals(other.ksiazki))
			return false;
		if (nazwa == null) {
			if (other.nazwa != null)
				return false;
		} else if (!nazwa.equals(other.nazwa))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
