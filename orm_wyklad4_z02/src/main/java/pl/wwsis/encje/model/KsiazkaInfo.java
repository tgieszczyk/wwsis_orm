package pl.wwsis.encje.model;

import org.apache.commons.lang.builder.ToStringBuilder;

public class KsiazkaInfo {
	private String nazwaWydawnictwa;
	private String tytul;

	public KsiazkaInfo(String tytul, String nazwaWydawnictwa) {
		this.tytul = tytul;
		this.nazwaWydawnictwa = nazwaWydawnictwa;
	}

	public String getNazwaWydawnictwa() {
		return nazwaWydawnictwa;
	}

	public void setNazwaWydawnictwa(String nazwaWydawnictwa) {
		this.nazwaWydawnictwa = nazwaWydawnictwa;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
