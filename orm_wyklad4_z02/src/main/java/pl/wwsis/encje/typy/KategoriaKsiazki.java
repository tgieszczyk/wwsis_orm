package pl.wwsis.encje.typy;

/**
 * @author ORM
 *
 */
public enum KategoriaKsiazki {
	HORROR, SENSACYJNE, SIFI, POPULARNO_NAUKOWE, BAJKI, KOMEDIA, ROMANTICO;
}
