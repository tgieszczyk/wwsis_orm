package pl.wwsis.encje;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.REFRESH;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author ORM
 *
 */
@Entity
@Table(name = "AUTOR")

@NamedQueries({

	// 1.znajdz autorow po tytule ksiazki
	@NamedQuery(name = Autor.NAMED_QUERY_ZNAJD_PO_TYTULE_KSIAZKI, query = "SELECT a FROM Autor a INNER JOIN a.ksiazki k WHERE UPPER(k.tytul) = UPPER(:tytul)"),
	
	// 2. znajdz autorow ktorzy wydali wiecej niz 1 ksiazke
	@NamedQuery(name = Autor.NAMED_QUERY_ZNAJD_O_MIN_LICZBIE_WYDANYCH_KSIAZEK, query = "SELECT a FROM Autor a WHERE SIZE(a.ksiazki) >= :minLiczbaKsiazek"),
	
	// 3. Znadz autorow i liczbe wydanych ksiazek;
	@NamedQuery(name = Autor.NAMED_QUERY_ZNAJD_AUTOROW_I_LICZBE_WYDANYCH_KSIAZEK, query = "SELECT a, SIZE(a.ksiazki) FROM Autor a GROUP BY a.id"),
	
	// 4. znajdz kategorie autorow FIXME - jeszcze nie jest do konca dopracowane
	@NamedQuery(name = Autor.NAMED_QUERY_ZNAJD_KATEGORIE_AUTOROW, query = "SELECT a, SUM(k.kategoria) FROM Autor a inner join a.ksiazki k GROUP BY a.id")
	
})
public class Autor {

	public static final String NAMED_QUERY_ZNAJD_PO_TYTULE_KSIAZKI = "autor.znajdzPoTytuleKsiazki";
	public static final String NAMED_QUERY_ZNAJD_O_MIN_LICZBIE_WYDANYCH_KSIAZEK = "autor.znajdzOMinLiczbieWydanychKsiazek";
	public static final String NAMED_QUERY_ZNAJD_AUTOROW_I_LICZBE_WYDANYCH_KSIAZEK = "autor.znajdzAutorowILiczbeWydanychKsiazek";
	public static final String NAMED_QUERY_ZNAJD_KATEGORIE_AUTOROW = "autor.znajdzKategorieAutorow";
	
	@Id
	@SequenceGenerator(name = "GEN_SEQ_WSPOLNA", sequenceName = "SEQ_WSPOLNA", allocationSize= 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_SEQ_WSPOLNA")
	private long id;

	@ManyToMany(cascade = {REFRESH, MERGE, DETACH}, mappedBy="autorzy")
	private List<Ksiazka> ksiazki;
	private String imie;
	private String nazwisko;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Ksiazka> getKsiazki() {
		if (ksiazki == null) {
			ksiazki = new ArrayList<>();
		}
		return ksiazki;
	}

	public void setKsiazki(List<Ksiazka> ksiazki) {
		this.ksiazki = ksiazki;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((imie == null) ? 0 : imie.hashCode());
		result = prime * result
				+ ((nazwisko == null) ? 0 : nazwisko.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Autor other = (Autor) obj;
		if (id != other.id)
			return false;
		if (imie == null) {
			if (other.imie != null)
				return false;
		} else if (!imie.equals(other.imie))
			return false;
		if (nazwisko == null) {
			if (other.nazwisko != null)
				return false;
		} else if (!nazwisko.equals(other.nazwisko))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("\n\tAutor [id: %d, imie: %s, nazwisko: %s]", id,
				imie, nazwisko);
	}
}
