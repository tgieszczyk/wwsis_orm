package pl.wwsis.encje;

import static javax.persistence.CascadeType.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import pl.wwsis.encje.typy.KategoriaKsiazki;

@Entity
@Table(name = "KSIAZKA")
@NamedQueries({
		@NamedQuery(name = Ksiazka.NAMED_QUERY_ZANJDZ_KSIAZLI_PO_LICZBIE_AUTOROW, query = "SELECT k FROM Ksiazka k WHERE SIZE(k.autorzy) = :liczbaAutorow"),

		// znajdz ksiazki po kategorii
		// jezeli chcemy wyciagnac autora z ksiazki to musimy wykonac inner join
		// i wowcza mozemy go zwrocic jako obiekt
		@NamedQuery(name = Ksiazka.NAMED_QUERY_ZNAJDZ_PO_KATEGORII, query = "SELECT k, a FROM Ksiazka k INNER JOIN k.autorzy a WHERE k.kategoria IN (:kategorie)"),

		@NamedQuery(name = Ksiazka.NAMED_QUERY_ZNAJDZ_INFORMACJE, query = "SELECT new pl.wwsis.encje.model.KsiazkaInfo(k.tytul, k.wydawnictwo.nazwa) FROM Ksiazka k") })
// @NamedNativeQueries({
// @NamedNativeQuery()
// })
public class Ksiazka {

	public static final String NAMED_QUERY_ZANJDZ_KSIAZLI_PO_LICZBIE_AUTOROW = "ksiazka.znajdzKsiazkiZOkresLLiczbaAutorow";
	public static final String NAMED_QUERY_ZNAJDZ_PO_KATEGORII = "ksiazka.znajdzKsiazkiPoKategorii";
	public static final String NAMED_QUERY_ZNAJDZ_INFORMACJE = "ksiazka.znajdzKsiazkiInfo";

	@Id
	@SequenceGenerator(name = "GEN_SEQ_WSPOLNA", sequenceName = "SEQ_WSPOLNA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_SEQ_WSPOLNA")
	private long id;

	@ManyToMany(cascade = { PERSIST, REFRESH, MERGE, DETACH })
	@JoinTable(name = "KSIAZKA_AUTOR", inverseJoinColumns = {
			@JoinColumn(name = "AUTOR_ID", referencedColumnName = "ID") }, joinColumns = {
					@JoinColumn(referencedColumnName = "ID", name = "KSIAZKA_ID") })
	private List<Autor> autorzy;

	@ManyToOne(cascade = { REFRESH, MERGE, DETACH })
	@JoinColumn(name = "WYDAWNICTWO_ID", referencedColumnName = "ID")
	private Wydawnictwo wydawnictwo;

	private String tytul;
	private String opis;
	@Column(name = "DATA_WYDANIA")
	@Temporal(TemporalType.DATE)
	private Date dataWydania;
	@Column(name = "KATEGORIA")
	@Enumerated(EnumType.STRING)
	private KategoriaKsiazki kategoria;

	@Column(name = "CENA")
	private Double cena;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Autor> getAutorzy() {
		if (autorzy == null) {
			autorzy = new ArrayList<>();
		}
		return autorzy;
	}

	public void setAutorzy(List<Autor> autorzy) {
		this.autorzy = autorzy;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Date getDataWydania() {
		return dataWydania;
	}

	public void setDataWydania(Date dataWydania) {
		this.dataWydania = dataWydania;
	}

	public KategoriaKsiazki getKategoria() {
		return kategoria;
	}

	public void setKategoria(KategoriaKsiazki kategoria) {
		this.kategoria = kategoria;
	}

	public Wydawnictwo getWydawnictwo() {
		return wydawnictwo;
	}

	public void setWydawnictwo(Wydawnictwo wydawnictwo) {
		this.wydawnictwo = wydawnictwo;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autorzy == null) ? 0 : autorzy.hashCode());
		result = prime * result + ((dataWydania == null) ? 0 : dataWydania.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((kategoria == null) ? 0 : kategoria.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((tytul == null) ? 0 : tytul.hashCode());
		result = prime * result + ((wydawnictwo == null) ? 0 : wydawnictwo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ksiazka other = (Ksiazka) obj;
		if (autorzy == null) {
			if (other.autorzy != null)
				return false;
		} else if (!autorzy.equals(other.autorzy))
			return false;
		if (dataWydania == null) {
			if (other.dataWydania != null)
				return false;
		} else if (!dataWydania.equals(other.dataWydania))
			return false;
		if (id != other.id)
			return false;
		if (kategoria != other.kategoria)
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		if (tytul == null) {
			if (other.tytul != null)
				return false;
		} else if (!tytul.equals(other.tytul))
			return false;
		if (wydawnictwo == null) {
			if (other.wydawnictwo != null)
				return false;
		} else if (!wydawnictwo.equals(other.wydawnictwo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

}
