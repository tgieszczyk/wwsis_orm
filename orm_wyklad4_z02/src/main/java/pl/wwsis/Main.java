package pl.wwsis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.encje.Adres;
import pl.wwsis.encje.Autor;
import pl.wwsis.encje.Ksiazka;
import pl.wwsis.encje.Wydawnictwo;
import pl.wwsis.encje.typy.KategoriaKsiazki;

public class Main {
	private static SimpleDateFormat date_formatter = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String[] args) throws Exception {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		// jesteśmy gotowi aby tworzyć encję

		EntityTransaction transaction = em.getTransaction();
		
		Wydawnictwo w = createWydawnictwo();
		
		Autor[] autorzyWBazie = new Autor[] {
				createAutor("Tomasz", "Gieszczyk"), 
				createAutor("Mariusz", "Waszak"), 
				createAutor("Katarzyna", "Pentos"), 
				createAutor("Sam", "Ruby"), 
				createAutor("Leonard", "Richardson")};
		
		w.getAutorzy().addAll(Arrays.asList(autorzyWBazie));
		
		
		Ksiazka[] ksiazkiWBazie = new Ksiazka[]{
				createKsiazka("JPA 2.0 - for professionals", "Uczy, edukuje, bawi", date_formatter.parse("2014-01-01"), KategoriaKsiazki.POPULARNO_NAUKOWE, w, autorzyWBazie[0],autorzyWBazie[4]),
				createKsiazka("Java for beginers", "learn how to programm in java language", date_formatter.parse("2015-10-01"), KategoriaKsiazki.POPULARNO_NAUKOWE, w, autorzyWBazie[1],autorzyWBazie[0], autorzyWBazie[3]),
				createKsiazka("Czerwony Kapturek", "Uczy, bawi, straszy- tutorial dla wilka", date_formatter.parse("2013-10-01"), KategoriaKsiazki.BAJKI, w, autorzyWBazie[2], autorzyWBazie[1])};

		try {

			transaction.begin();

			em.persist(w);
			em.flush();

			for (Ksiazka k : ksiazkiWBazie) {
				em.persist(k);
			}

			// wykonujemy operacje create, delete, update
			// em.persist(p);
			// em.remove
			// em.merge

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
		System.exit(0);
	}

	public static Wydawnictwo createWydawnictwo() {
		Adres a = new Adres();
		a.setKodPocztowy("54-430");
		a.setKraj("Polska");
		a.setMiasto("Wroclaw");
		a.setUlica("Wejherowska");

		Wydawnictwo w = new Wydawnictwo();
		w.setAdres(a);
		w.setNazwa("WWSIS -JWD");

		return w;
	}

	public static Autor createAutor(String imie, String nazwisko) {
		Autor a = new Autor();
		a.setImie(imie);
		a.setNazwisko(nazwisko);
		return a;
	}

	public static Ksiazka createKsiazka(String tytul, String opis, Date dataWydania, KategoriaKsiazki kategoria, Wydawnictwo wydawnictwo,
			Autor... autorzy) {
		
		Ksiazka k = new Ksiazka();
		k.setDataWydania(dataWydania);
		k.setKategoria(kategoria);
		k.setOpis(opis);
		k.setTytul(tytul);
		k.getAutorzy().addAll(Arrays.asList(autorzy));
		k.setWydawnictwo(wydawnictwo);

		return k;
	}
}
