package pl.wwsis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pl.wwsis.encje.Adres;
import pl.wwsis.encje.Autor;
import pl.wwsis.encje.Ksiazka;
import pl.wwsis.encje.Wydawnictwo;
import pl.wwsis.encje.typy.KategoriaKsiazki;

public class MainQueryApi {
	private static SimpleDateFormat date_formatter = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String[] args) throws Exception {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();
//		Autor autorBezKsiazki = Main.createAutor("Autor", "BezKsiazki2");

		try {
			
//			Wydawnictwo w = em.find(Wydawnictwo.class, 1L);
//			w.getAutorzy().add(autorBezKsiazki);
//			
//			transaction.begin();
//			em.merge(w);
//			transaction.commit();
			
			
			// 1.
//			Query q = em.createNamedQuery(Ksiazka.NAMED_QUERY_ZANJDZ_KSIAZLI_PO_LICZBIE_AUTOROW);
//			q.setParameter("liczbaAutorow", 1);
			
			// 2. 
//			Query q = em.createNamedQuery(Ksiazka.NAMED_QUERY_ZNAJDZ_PO_KATEGORII);
//			q.setParameter("kategorie", Arrays.asList(KategoriaKsiazki.BAJKI, KategoriaKsiazki.POPULARNO_NAUKOWE));
			
			// 3.
//			Query q = em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_PO_TYTULE_KSIAZKI);
//			q.setParameter("tytul", "JPA 2.0 - for professionals");
			
			// 4.
//			Query q = em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_O_MIN_LICZBIE_WYDANYCH_KSIAZEK);
//			q.setParameter("minLiczbaKsiazek", 2);
			
			// 5.
//			Query q = em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_AUTOROW_I_LICZBE_WYDANYCH_KSIAZEK);
			
			// 6. FIXME
//			Query q = em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_KATEGORIE_AUTOROW);
			
			// 7. wyciaganie gotowych obiektow
			Query q = em.createNamedQuery(Ksiazka.NAMED_QUERY_ZNAJDZ_INFORMACJE);
			
			
			// wypisywanie resultatow
			List<Object> result = q.getResultList();
			
			System.out.println(result);

		} catch (Exception e) {
			e.printStackTrace();
			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
		System.exit(0);
	}

}
