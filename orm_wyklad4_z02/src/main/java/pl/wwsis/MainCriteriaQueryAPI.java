package pl.wwsis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import com.sun.jndi.cosnaming.IiopUrl.Address;

import pl.wwsis.encje.Adres;
import pl.wwsis.encje.Autor;
import pl.wwsis.encje.Ksiazka;
import pl.wwsis.encje.Wydawnictwo;
import pl.wwsis.encje.typy.KategoriaKsiazki;

public class MainCriteriaQueryAPI {
	private static SimpleDateFormat date_formatter = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String[] args) throws Exception {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		CriteriaQuery<Ksiazka> ksiazkaQuery = cb.createQuery(Ksiazka.class);
		Root<Ksiazka> ksiazkaFrom = ksiazkaQuery.from(Ksiazka.class);

//		CriteriaQuery<Adres> adressQuery = cb.createQuery(Adres.class);
//		Root<Adres> addressFrom = adressQuery.from(Adres.class);
		
 
//		ParameterExpression<Integer> p = cb.parameter(Integer.class);
//
//		adressQuery.select(adressQuery.from(Adres.class)).where(cb.greaterThan(rootAddress.get("numerUlicy"), p));
		

		EntityTransaction transaction = em.getTransaction();
		// Autor autorBezKsiazki = Main.createAutor("Autor", "BezKsiazki2");

		try {
			
			// 1. wyciagamy ksiazki drozsze od 40 zl
			// from Ksiazka where cena > :cena
//			ksiazkaQuery.select(ksiazkaFrom).where(cb.greaterThan(ksiazkaFrom.get("cena"), 40));
			// lub przez parametr
			ParameterExpression<Double> cenaParametr = cb.parameter(Double.class, "cena");
			ksiazkaQuery.select(ksiazkaFrom).where(cb.greaterThan(ksiazkaFrom.get("cena"), cenaParametr));
			
			Query q = em.createQuery(ksiazkaQuery).setParameter("cena", 40D); 
			
			// 2. 
			//Query q =
						// em.createNamedQuery(Ksiazka.NAMED_QUERY_ZANJDZ_KSIAZLI_PO_LICZBIE_AUTOROW);
						// q.setParameter("liczbaAutorow", 1);
			// SELECT k FROM Ksiazka k WHERE SIZE(k.autorzy) = :liczbaAutorow
//			ksiazkaQuery
//				.select(ksiazkaFrom)
//				// where c1 and (c2 and (..))
//				// where (conditio1 or condition2...) and.. and... 
////				.where(cb.and(cb.or(restrictions), cb.size(collection))
//				.where(
//						cb.equal(
//								cb.size(ksiazkaFrom.get("autorzy")), 3
//								)
//						);
			

			// 2.
			// Query q =
			// em.createNamedQuery(Ksiazka.NAMED_QUERY_ZNAJDZ_PO_KATEGORII);
			// q.setParameter("kategorie", Arrays.asList(KategoriaKsiazki.BAJKI,
			// KategoriaKsiazki.POPULARNO_NAUKOWE));

			// 3.
			// Query q =
			// em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_PO_TYTULE_KSIAZKI);
			// q.setParameter("tytul", "JPA 2.0 - for professionals");

			// 4.
			// Query q =
			// em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_O_MIN_LICZBIE_WYDANYCH_KSIAZEK);
			// q.setParameter("minLiczbaKsiazek", 2);

			// 5.
			// Query q =
			// em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_AUTOROW_I_LICZBE_WYDANYCH_KSIAZEK);

//			Query q = em.createQuery(ksiazkaQuery);
			
			// wypisywanie resultatow
			List<Object> result = q.getResultList();

			System.out.println(result);

		} catch (Exception e) {
			e.printStackTrace();
			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
		System.exit(0);
	}

}
