package pl.wwsis.wyklad2.p05;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class DodatkoweInformacje {

	private String opis;
	@Transient
	private boolean nowyUzytkownik = false;
	private String adres;

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public boolean isNowyUzytkownik() {
		return nowyUzytkownik;
	}

	public void setNowyUzytkownik(boolean nowyUzytkownik) {
		this.nowyUzytkownik = nowyUzytkownik;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

}
