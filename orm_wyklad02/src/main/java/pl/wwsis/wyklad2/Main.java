package pl.wwsis.wyklad2;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.wyklad2.encje.Product;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		// jesteśmy gotowi aby tworzyć encję

		EntityTransaction transaction = em.getTransaction();

		try {
			Product p = new Product();
			p.setCena(new BigDecimal(20.56d));
			p.setDataDodania(new Date());
			p.setNazwa("Klapa przednia");
			p.setOpis("Opis testowy");
			p.setwOfercie(true);

			transaction.begin();

			em.persist(p);

			transaction.commit();
			System.exit(0);
		} catch (Exception e) {

			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}

}
