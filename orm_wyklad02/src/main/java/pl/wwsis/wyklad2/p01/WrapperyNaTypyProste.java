package pl.wwsis.wyklad2.p01;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WrapperyNaTypyProste implements Serializable {

	private static final long serialVersionUID = 7662187375724840L;
	@Id
	private Long id;
	private Float cena;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Float getCena() {
		return cena;
	}
	public void setCena(Float cena) {
		this.cena = cena;
	}


}
