package pl.wwsis.wyklad2.p02;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class Uzytkownik {
	@Id
	private long id;

	@Enumerated(EnumType.STRING)
	// Je�eli pole ma warto�� GOSC to w bazie danych w kolumnie typUzytkownika
	// b�dzie zapisana warto�� String GOSC
	private TypUzytkownika typUzytkownika;
	@Enumerated(EnumType.ORDINAL)
	// Je�eli pole ma warto�� GOSC to w bazie danych w kolumnie alternatywnyTyp
	// b�dzie zapisana warto�� 3, gdy� taki index posiada ta warto�� w typie
	// TypUzytkownika
	private TypUzytkownika alternatywnyTyp;

	private String name;
	private String nip;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TypUzytkownika getTypUzytkownika() {
		return typUzytkownika;
	}

	public void setTypUzytkownika(TypUzytkownika typUzytkownika) {
		this.typUzytkownika = typUzytkownika;
	}

	public TypUzytkownika getAlternatywnyTyp() {
		return alternatywnyTyp;
	}

	public void setAlternatywnyTyp(TypUzytkownika alternatywnyTyp) {
		this.alternatywnyTyp = alternatywnyTyp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

}
