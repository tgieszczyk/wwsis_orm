package pl.wwsis.wyklad2.p04;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class ZdjecieTest {

	public static void main(String[] args) throws Exception {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();

			Zdjecie z = new Zdjecie();
			z.setId(1);
			z.setIdUzytkownika(1);
			z.setZdjecie(loadImage("/pl/wwsis/wyklad2/p04/zdjecie.JPG"));
			z.setOpis(generateVeryLongDescription(2000));

			em.persist(z);
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
	}

	private static String generateVeryLongDescription(int size) {
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < size;) {
			try {
				b.append(new Random().nextInt());
			} finally {
				i = b.length();
			}
		}
		return b.toString();
	}

	private static byte[] loadImage(String path) throws Exception {
		InputStream is = null;
		BufferedInputStream bis = null;
		ByteArrayOutputStream baos = null;

		try {
			is = ZdjecieTest.class.getResourceAsStream(path);
			bis = new BufferedInputStream(is);
			baos = new ByteArrayOutputStream();
			byte[] buff = new byte[1024];

			int len = 0;

			while ((len = bis.read(buff)) > 0) {
				baos.write(buff, 0, len);
				baos.flush();
			}
			return baos.toByteArray();
		} finally {
			close(is, bis, baos);
		}
	}

	private static <T extends Closeable> void close(
			@SuppressWarnings("unchecked") T... closeables) {
		for (T t : closeables) {
			try {
				if (t != null) {
					t.close();
				}
			} catch (Exception e) {
				// ignore
			}
		}
	}

}
