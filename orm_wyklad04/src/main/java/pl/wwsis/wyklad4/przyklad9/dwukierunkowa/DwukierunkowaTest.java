package pl.wwsis.wyklad4.przyklad9.dwukierunkowa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DwukierunkowaTest {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();

		try {
			KlientBi k = new KlientBi();
			k.setAktywny(true);
			k.setEmail("tgieszczyk@gmail.com");
			k.setNazwa("TMG Software Tomasz Gieszczyk");
			k.setNip("556556778");
			k.setTelefon("8898876");
			k.setWww("http://www.tmg-software.pl");
			
			AdresBi a = new AdresBi();
			a.setKlient(k);
			a.setKodPocztowy("53-102");
			a.setMiasto("Wrocław");
			a.setUlica("Wejherowska");
			
			transaction.begin();
			
			em.persist(k);
			
			a.setId(k.getId());
			k.setAdres(a);
			
			em.merge(k);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}

		
		try {
			long adresBiId = 1L;
			
			AdresBi wynik = em.find(AdresBi.class, adresBiId);
			System.out.println(String.format("Adres: %s", wynik.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.exit(0);
	}

}
