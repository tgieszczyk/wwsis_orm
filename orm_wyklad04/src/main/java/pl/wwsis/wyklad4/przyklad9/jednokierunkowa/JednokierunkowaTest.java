package pl.wwsis.wyklad4.przyklad9.jednokierunkowa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class JednokierunkowaTest {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();

		KlientUni k = new KlientUni();

		k.setAktywny(true);
		k.setEmail("tgieszczyk@gmail.com");
		k.setNazwa("TMG Software Tomasz Gieszczyk");
		k.setNip("556556778");
		k.setTelefon("8898876");
		k.setWww("http://www.tmg-software.pl");

		AdresUni a = new AdresUni();
		a.setKodPocztowy("53-102");
		a.setMiasto("Wrocław");
		a.setUlica("Wejherowska");
		try {

			transaction.begin();

			em.persist(k);
			a.setId(k.getId());

			k.setAdres(a);
			em.merge(k);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}

		long klientUniId = 1L;

		KlientUni wynik = em.find(KlientUni.class, klientUniId);
		System.out.println(String.format("Klient: %s", wynik.toString()));

		try {
			transaction.begin();

			em.remove(k);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
	}

}
