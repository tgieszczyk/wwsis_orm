package pl.wwsis.wyklad4.przyklad9.dwukierunkowa;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ADRES_BI_DIR")
public class AdresBi {
	@Id
	@Column(name = "id")
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String ulica;
	private String kodPocztowy;
	private String miasto;

	@OneToOne(mappedBy = "adres", optional = false, 
			cascade = { PERSIST, MERGE, REFRESH })
	private KlientBi klient;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getKodPocztowy() {
		return kodPocztowy;
	}

	public void setKodPocztowy(String kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}

	public String getMiasto() {
		return miasto;
	}

	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}

	public KlientBi getKlient() {
		return klient;
	}

	public void setKlient(KlientBi klient) {
		this.klient = klient;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result
				+ ((kodPocztowy == null) ? 0 : kodPocztowy.hashCode());
		result = prime * result + ((miasto == null) ? 0 : miasto.hashCode());
		result = prime * result + ((ulica == null) ? 0 : ulica.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdresBi other = (AdresBi) obj;
		if (id != other.id)
			return false;
		if (kodPocztowy == null) {
			if (other.kodPocztowy != null)
				return false;
		} else if (!kodPocztowy.equals(other.kodPocztowy))
			return false;
		if (miasto == null) {
			if (other.miasto != null)
				return false;
		} else if (!miasto.equals(other.miasto))
			return false;
		if (ulica == null) {
			if (other.ulica != null)
				return false;
		} else if (!ulica.equals(other.ulica))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String
				.format("Adres [id:%d, ulica: %s, kodPocztowy: %s, miasto: %s, klientId: %d]",
						this.id, ulica, kodPocztowy, miasto, klient.getId());
	}
}
