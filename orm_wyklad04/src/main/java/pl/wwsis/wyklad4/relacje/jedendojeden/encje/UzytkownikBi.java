package pl.wwsis.wyklad4.relacje.jedendojeden.encje;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "UZYTKOWNIK_JEDEN_DO_JEDEN_BI")
public class UzytkownikBi {
	@Id
	private long id;
	private String nazwa;
	private String nip;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "adres_id", referencedColumnName = "id")
	private AdresUzytkownikaBi adres;

	private String telefon;
	private String email;
	private String www;
	private boolean aktywny;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public AdresUzytkownikaBi getAdres() {
		return adres;
	}

	public void setAdres(AdresUzytkownikaBi adres) {
		this.adres = adres;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWww() {
		return www;
	}

	public void setWww(String www) {
		this.www = www;
	}

	public boolean isAktywny() {
		return aktywny;
	}

	public void setAktywny(boolean aktywny) {
		this.aktywny = aktywny;
	}

}
