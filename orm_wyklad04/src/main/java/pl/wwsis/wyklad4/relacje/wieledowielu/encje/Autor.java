package pl.wwsis.wyklad4.relacje.wieledowielu.encje;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AUTOR")
public class Autor {
	@Id
	@GeneratedValue
	private long id;

	@ManyToMany(cascade = { PERSIST, REFRESH, MERGE, DETACH })
	@JoinTable(name = "KSIAZKA_AUTOR", 
		joinColumns = { 
			@JoinColumn(name = "AUTOR_ID", referencedColumnName = "id") 
		}, 
		inverseJoinColumns = { 
			@JoinColumn(referencedColumnName = "id", name = "KSIAZKA_ID")
		}
	)
	private List<Ksiazka> ksiazki;
	private String imie;
	private String nazwisko;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Ksiazka> getKsiazki() {
		if (ksiazki == null) {
			ksiazki = new ArrayList<>();
		}
		return ksiazki;
	}

	public void setKsiazki(List<Ksiazka> ksiazki) {
		this.ksiazki = ksiazki;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((imie == null) ? 0 : imie.hashCode());
		result = prime * result
				+ ((nazwisko == null) ? 0 : nazwisko.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Autor other = (Autor) obj;
		if (id != other.id)
			return false;
		if (imie == null) {
			if (other.imie != null)
				return false;
		} else if (!imie.equals(other.imie))
			return false;
		if (nazwisko == null) {
			if (other.nazwisko != null)
				return false;
		} else if (!nazwisko.equals(other.nazwisko))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("\n\tAutor [id: %d, imie: %s, nazwisko: %s]", id,
				imie, nazwisko);
	}
}
