package pl.wwsis.wyklad4.relacje.jedendowielu.encje;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "UZYTKOWNIK_JEDEN_DO_WIELU")
public class Uzytkownik {
	@Id
	@GeneratedValue
	private long id;
	private String nazwa;
	private String nip;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "uzytkownik_id")
	private List<Adres> adresList;

	private String telefon;
	private String email;
	private String www;
	private boolean aktywny;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public List<Adres> getAdresList() {
		return adresList;
	}

	public void setAdresList(List<Adres> adresList) {
		this.adresList = adresList;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWww() {
		return www;
	}

	public void setWww(String www) {
		this.www = www;
	}

	public boolean isAktywny() {
		return aktywny;
	}

	public void setAktywny(boolean aktywny) {
		this.aktywny = aktywny;
	}
}
