package pl.wwsis.wyklad4.relacje.wieledowielu.encje;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class WieleDoWieluTest {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit-postgresql");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();

		try {
			Autor[] autorzy = { newAutor("Marcin", "Nowak"),
					newAutor("Tomasz", "Gieszczyk"),
					newAutor("Bronek", "Komorek"), newAutor("Donald", "Duck") };

			Ksiazka k1 = newKsiazka("Guliwer", "Ksiązka dla dzieci",
					autorzy[0], autorzy[1]);
			Ksiazka k2 = newKsiazka("Kopciuszek na skraku jutra", "Fairytail",
					autorzy[1], autorzy[2]);
			Ksiazka k3 = newKsiazka("JPA 2.1 for dummies",
					"Jak nie popaść w pułapki mapowania modelu bazodanowego",
					autorzy[0], autorzy[1], autorzy[2]);
			Ksiazka k4 = newKsiazka("Java dla zawansowanych",
					"Nic dodać nic ująć", autorzy[2], autorzy[3]);
			transaction.begin();
			em.persist(k1);
			em.persist(k2);
			em.persist(k3);
			em.persist(k4);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}

		// long adresBiId = 2L;
		//
		// AdresBi wynik = em.find(AdresBi.class, adresBiId);
		// System.out.println(String.format("Adres: %s", wynik.toString()));

		@SuppressWarnings("unchecked")
		List<Ksiazka> ksiazki = em.createQuery("FROM Ksiazka").getResultList();
		
		System.out.println(ksiazki);
		System.exit(0);
	}

	private static Autor newAutor(String imie, String nazwisko) {
		Autor result = new Autor();
		result.setImie(imie);
		result.setNazwisko(nazwisko);
		return result;
	}

	private static Ksiazka newKsiazka(String tytul, String opis,
			Autor... autorzy) {
		Ksiazka k = new Ksiazka();

		k.setOpis(opis);
		k.setTytul(tytul);

		for (Autor a : autorzy) {
			k.getAutorzy().add(a);
			a.getKsiazki().add(k);
		}

		return k;
	}

}
