package pl.wwsis.wyklad4.relacje.wieledowielu.encje;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "KSIAZKA")
public class Ksiazka {
	@Id
	@GeneratedValue
	private long id;

	@ManyToMany(mappedBy = "ksiazki", cascade = { PERSIST, REFRESH, MERGE,
			DETACH })
	private List<Autor> autorzy;

	private String tytul;
	private String opis;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Autor> getAutorzy() {
		if (autorzy == null) {
			autorzy = new ArrayList<>();
		}
		return autorzy;
	}

	public void setAutorzy(List<Autor> autorzy) {
		this.autorzy = autorzy;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autorzy == null) ? 0 : autorzy.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((tytul == null) ? 0 : tytul.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ksiazka other = (Ksiazka) obj;
		if (autorzy == null) {
			if (other.autorzy != null)
				return false;
		} else if (!autorzy.equals(other.autorzy))
			return false;
		if (id != other.id)
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		if (tytul == null) {
			if (other.tytul != null)
				return false;
		} else if (!tytul.equals(other.tytul))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("Ksiazka [id: %d, tytul: %s, autorzy: %s]", id,
				tytul, getAutorzy().toString());
	}

}
