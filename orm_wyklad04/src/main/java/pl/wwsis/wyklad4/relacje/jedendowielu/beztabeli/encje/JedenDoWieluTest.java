package pl.wwsis.wyklad4.relacje.jedendowielu.beztabeli.encje;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class JedenDoWieluTest {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit-postgresql");

		EntityManager em = factory.createEntityManager();

		EntityTransaction transaction = em.getTransaction();

		try {
			Klient k = new Klient();
			k.setEmail("tgieszczyk@gmail.com");
			k.setNazwa("TMG Software Tomasz Gieszczyk");
			k.setNip("556556778");
			k.setWww("http://www.tmg-software.pl");
			
			for(int i=0; i<4; i++) {
				Zamowienie z = new Zamowienie();
				z.setNumerEwidencyjny("2015 / 02 / " + i);
				k.getZamowienia().add(z);
			}
			
			transaction.begin();
			
			em.persist(k);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}

		
//		long adresBiId = 2L;
//		
//		AdresBi wynik = em.find(AdresBi.class, adresBiId);
//		System.out.println(String.format("Adres: %s", wynik.toString()));
		
		System.exit(0);
	}

}
