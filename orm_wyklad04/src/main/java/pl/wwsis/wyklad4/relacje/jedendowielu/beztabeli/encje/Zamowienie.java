package pl.wwsis.wyklad4.relacje.jedendowielu.beztabeli.encje;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ZAMOWIENIE_JEDEN_DO_WIELU")
public class Zamowienie {

	@Id
	@GeneratedValue
	private long id;
	
	private String numerEwidencyjny;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumerEwidencyjny() {
		return numerEwidencyjny;
	}

	public void setNumerEwidencyjny(String numerEwidencyjny) {
		this.numerEwidencyjny = numerEwidencyjny;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime
				* result
				+ ((numerEwidencyjny == null) ? 0 : numerEwidencyjny.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zamowienie other = (Zamowienie) obj;
		if (id != other.id)
			return false;
		if (numerEwidencyjny == null) {
			if (other.numerEwidencyjny != null)
				return false;
		} else if (!numerEwidencyjny.equals(other.numerEwidencyjny))
			return false;
		return true;
	}

	
}
