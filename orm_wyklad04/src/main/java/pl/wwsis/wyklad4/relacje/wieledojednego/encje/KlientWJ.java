package pl.wwsis.wyklad4.relacje.wieledojednego.encje;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "KLIENT_WIELE_DO_JEDNEGO")
public class KlientWJ {
	@Id
	@GeneratedValue
	private long id;

	@OneToMany(mappedBy = "klient", cascade = CascadeType.ALL)
	private List<ZamowienieWJ> zamowienia;

	private String nazwa;
	private String nip;
	private String www;
	private String email;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getWww() {
		return www;
	}

	public void setWww(String www) {
		this.www = www;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<ZamowienieWJ> getZamowienia() {
		if (zamowienia == null) {
			zamowienia = new ArrayList<>();
		}
		return zamowienia;
	}

	public void setZamowienia(List<ZamowienieWJ> zamowienia) {
		this.zamowienia = zamowienia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		result = prime * result + ((nip == null) ? 0 : nip.hashCode());
		result = prime * result + ((www == null) ? 0 : www.hashCode());
		result = prime * result
				+ ((zamowienia == null) ? 0 : zamowienia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KlientWJ other = (KlientWJ) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (nazwa == null) {
			if (other.nazwa != null)
				return false;
		} else if (!nazwa.equals(other.nazwa))
			return false;
		if (nip == null) {
			if (other.nip != null)
				return false;
		} else if (!nip.equals(other.nip))
			return false;
		if (www == null) {
			if (other.www != null)
				return false;
		} else if (!www.equals(other.www))
			return false;
		if (zamowienia == null) {
			if (other.zamowienia != null)
				return false;
		} else if (!zamowienia.equals(other.zamowienia))
			return false;
		return true;
	}

}
