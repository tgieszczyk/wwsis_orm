package pl.wwsis.tableperclass;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import pl.wwsis.typy.TypOsobowy;

/**
 * @author ORM
 *
 */
@Entity
@Table(name = "TPC_OSOBOWY")
public class SamochodOsoboowyTablePClass extends PojazdTablePClass {
	private int mksymalnaLiczbaPasazerow;
	private int liczbaDrzwi;
	@Enumerated(EnumType.STRING)
	private TypOsobowy typ;
	public int getMksymalnaLiczbaPasazerow() {
		return mksymalnaLiczbaPasazerow;
	}
	public void setMksymalnaLiczbaPasazerow(int mksymalnaLiczbaPasazerow) {
		this.mksymalnaLiczbaPasazerow = mksymalnaLiczbaPasazerow;
	}
	public int getLiczbaDrzwi() {
		return liczbaDrzwi;
	}
	public void setLiczbaDrzwi(int liczbaDrzwi) {
		this.liczbaDrzwi = liczbaDrzwi;
	}
	public TypOsobowy getTyp() {
		return typ;
	}
	public void setTyp(TypOsobowy typ) {
		this.typ = typ;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + liczbaDrzwi;
		result = prime * result + mksymalnaLiczbaPasazerow;
		result = prime * result + ((typ == null) ? 0 : typ.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SamochodOsoboowyTablePClass other = (SamochodOsoboowyTablePClass) obj;
		if (liczbaDrzwi != other.liczbaDrzwi)
			return false;
		if (mksymalnaLiczbaPasazerow != other.mksymalnaLiczbaPasazerow)
			return false;
		if (typ != other.typ)
			return false;
		return true;
	}
	
	
}
