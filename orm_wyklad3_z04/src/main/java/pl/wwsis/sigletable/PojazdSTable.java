package pl.wwsis.sigletable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import pl.wwsis.typy.RodzajSilnika;

/**
 * @author ORM
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "SINGLETABLE_POJAZD")
@DiscriminatorColumn(name = "RODZAJ_POJAZDU")
public class PojazdSTable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String marka;
	@Enumerated(EnumType.STRING)
	private RodzajSilnika rodzajSilnika;
	private double pojemnosc;
	private double srednieSpalaniePer100km;
	private int predkoscMaksynalna;
	private double przyspieszenie;
	private double mocKW;
	// moment obrotowy
	private double nmp;
	private String kolor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public RodzajSilnika getRodzajSilnika() {
		return rodzajSilnika;
	}

	public void setRodzajSilnika(RodzajSilnika rodzajSilnika) {
		this.rodzajSilnika = rodzajSilnika;
	}

	public double getPojemnosc() {
		return pojemnosc;
	}

	public void setPojemnosc(double pojemnosc) {
		this.pojemnosc = pojemnosc;
	}

	public double getSrednieSpalaniePer100km() {
		return srednieSpalaniePer100km;
	}

	public void setSrednieSpalaniePer100km(double srednieSpalaniePer100km) {
		this.srednieSpalaniePer100km = srednieSpalaniePer100km;
	}

	public int getPredkoscMaksynalna() {
		return predkoscMaksynalna;
	}

	public void setPredkoscMaksynalna(int predkoscMaksynalna) {
		this.predkoscMaksynalna = predkoscMaksynalna;
	}

	public double getPrzyspieszenie() {
		return przyspieszenie;
	}

	public void setPrzyspieszenie(double przyspieszenie) {
		this.przyspieszenie = przyspieszenie;
	}

	public double getMocKW() {
		return mocKW;
	}

	public void setMocKW(double mocKW) {
		this.mocKW = mocKW;
	}

	public double getNmp() {
		return nmp;
	}

	public void setNmp(double nmp) {
		this.nmp = nmp;
	}

	public String getKolor() {
		return kolor;
	}

	public void setKolor(String kolor) {
		this.kolor = kolor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((kolor == null) ? 0 : kolor.hashCode());
		result = prime * result + ((marka == null) ? 0 : marka.hashCode());
		long temp;
		temp = Double.doubleToLongBits(mocKW);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(nmp);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(pojemnosc);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + predkoscMaksynalna;
		temp = Double.doubleToLongBits(przyspieszenie);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((rodzajSilnika == null) ? 0 : rodzajSilnika.hashCode());
		temp = Double.doubleToLongBits(srednieSpalaniePer100km);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PojazdSTable other = (PojazdSTable) obj;
		if (id != other.id)
			return false;
		if (kolor == null) {
			if (other.kolor != null)
				return false;
		} else if (!kolor.equals(other.kolor))
			return false;
		if (marka == null) {
			if (other.marka != null)
				return false;
		} else if (!marka.equals(other.marka))
			return false;
		if (Double.doubleToLongBits(mocKW) != Double.doubleToLongBits(other.mocKW))
			return false;
		if (Double.doubleToLongBits(nmp) != Double.doubleToLongBits(other.nmp))
			return false;
		if (Double.doubleToLongBits(pojemnosc) != Double.doubleToLongBits(other.pojemnosc))
			return false;
		if (predkoscMaksynalna != other.predkoscMaksynalna)
			return false;
		if (Double.doubleToLongBits(przyspieszenie) != Double.doubleToLongBits(other.przyspieszenie))
			return false;
		if (rodzajSilnika != other.rodzajSilnika)
			return false;
		if (Double.doubleToLongBits(srednieSpalaniePer100km) != Double.doubleToLongBits(other.srednieSpalaniePer100km))
			return false;
		return true;
	}

}
