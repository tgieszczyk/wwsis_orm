package pl.wwsis;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.joined.MotocyklJoined;
import pl.wwsis.joined.PojazdJoined;
import pl.wwsis.joined.SamochodCiezarowyJoined;
import pl.wwsis.joined.SamochodOsoboowyJoined;
import pl.wwsis.typy.RodzajSilnika;
import pl.wwsis.typy.TypLadownosci;
import pl.wwsis.typy.TypMotocykla;
import pl.wwsis.typy.TypOsobowy;

public class MainJoined {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		// jesteśmy gotowi aby tworzyć encję

		EntityTransaction transaction = em.getTransaction();

		try {
			PojazdJoined ciezarowy = createSamochodCiezarowyJoined();
			PojazdJoined osobowy = createSamochodOsobowyJoined();
			PojazdJoined motocykl = createMotorJoined();
			
			transaction.begin();

			// wykonujemy operacje create, delete, update
			//em.persist(p);
			// em.remove
			// em.merge

			em.persist(motocykl);
			em.persist(osobowy);
			em.persist(ciezarowy);

			transaction.commit();
			
			PojazdJoined szukanyPojazd = em.find(PojazdJoined.class, motocykl.getId());
			
			if(szukanyPojazd != null && szukanyPojazd instanceof MotocyklJoined) {
				obsluzMotocykl((MotocyklJoined) szukanyPojazd);
			}
			
			System.exit(0);
		} catch (Exception e) {

			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}
	
	private static void obsluzMotocykl(MotocyklJoined motocykl) {
		System.out.println();
		// zrob cos z motocyklem
	}
	
	private static PojazdJoined createSamochodOsobowyJoined() {
		SamochodOsoboowyJoined pojazd = new SamochodOsoboowyJoined();
		
		pojazd.setKolor("Czerwony");
		pojazd.setLiczbaDrzwi(5);
		pojazd.setMarka("Mazda CX5");
		pojazd.setMksymalnaLiczbaPasazerow(5);
		pojazd.setMocKW(120);
		pojazd.setPojemnosc(2500);
		pojazd.setNmp(400);
		pojazd.setPredkoscMaksynalna(220);
		pojazd.setPrzyspieszenie(12);
		pojazd.setRodzajSilnika(RodzajSilnika.BENZYNA);
		pojazd.setSrednieSpalaniePer100km(6.5);
		pojazd.setTyp(TypOsobowy.CROSSOVER);
		
		return pojazd;
	}
	
	private static PojazdJoined createSamochodCiezarowyJoined() {
		SamochodCiezarowyJoined pojazd = new SamochodCiezarowyJoined();
		
		pojazd.setKolor("Czerwony");
		pojazd.setMarka("VOLVO FH4");
		pojazd.setMocKW(120);
		pojazd.setPojemnosc(8500);
		pojazd.setNmp(400);
		pojazd.setPredkoscMaksynalna(150);
		pojazd.setPrzyspieszenie(25);
		pojazd.setRodzajSilnika(RodzajSilnika.DIESEL);
		pojazd.setSrednieSpalaniePer100km(19);
		pojazd.setTonaz(60);
		pojazd.setLadownosc(20000);
		pojazd.setTypLadownosci(TypLadownosci.NACZEPA);
		
		return pojazd;
	}
	
	private static PojazdJoined createMotorJoined() {
		MotocyklJoined pojazd = new MotocyklJoined();

		pojazd.setKolor("Czerwony");
		pojazd.setMarka("Suzuki Ninja");
		pojazd.setMocKW(120);
		pojazd.setPojemnosc(500);
		pojazd.setNmp(400);
		pojazd.setPredkoscMaksynalna(280);
		pojazd.setPrzyspieszenie(2);
		pojazd.setRodzajSilnika(RodzajSilnika.BENZYNA);
		pojazd.setSrednieSpalaniePer100km(5);
		pojazd.setTypMotocykla(TypMotocykla.SCIGACZ);
		return pojazd;
	}

}
