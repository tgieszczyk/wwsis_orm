package pl.wwsis.typy;

/**
 * @author ORM
 *
 */
public enum RodzajSilnika {
	DIESEL, BENZYNA, LPG, HYBRYDA, ELEKTRYCZNY;
}
