package pl.wwsis.typy;

/**
 * @author ORM
 *
 */
public enum TypLadownosci {
	NACZEPA, WYWROTKA, WLASNY, CZTEROOSKA, SZESCIOOSKA;
}
