package pl.wwsis.typy;

/**
 * @author ORM
 *
 */
public enum TypMotocykla {
	CROSS, SCIGACZ, CHOPER, MOTOROWER, SKUTER, TURYSTYCZNE;
}
