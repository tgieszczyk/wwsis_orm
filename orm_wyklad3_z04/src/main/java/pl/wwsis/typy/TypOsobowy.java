package pl.wwsis.typy;

/**
 * @author ORM
 *
 */
public enum TypOsobowy {
	KOMBI, SEDAN, CHACHBACK, COUPE, CABRIO, CROSSOVER;
}
