package pl.wwsis.joined;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import pl.wwsis.typy.TypLadownosci;

/**
 * @author ORM
 *
 */
@Entity
@Table(name = "JOINED_SAMOCHOD_CIEZAROWY")
public class SamochodCiezarowyJoined extends PojazdJoined {
	private double ladownosc;
	private double tonaz;
	@Enumerated(EnumType.STRING)
	private TypLadownosci typLadownosci;

	public double getLadownosc() {
		return ladownosc;
	}

	public void setLadownosc(double ladownosc) {
		this.ladownosc = ladownosc;
	}

	public double getTonaz() {
		return tonaz;
	}

	public void setTonaz(double tonaz) {
		this.tonaz = tonaz;
	}

	public TypLadownosci getTypLadownosci() {
		return typLadownosci;
	}

	public void setTypLadownosci(TypLadownosci typLadownosci) {
		this.typLadownosci = typLadownosci;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(ladownosc);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(tonaz);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((typLadownosci == null) ? 0 : typLadownosci.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SamochodCiezarowyJoined other = (SamochodCiezarowyJoined) obj;
		if (Double.doubleToLongBits(ladownosc) != Double.doubleToLongBits(other.ladownosc))
			return false;
		if (Double.doubleToLongBits(tonaz) != Double.doubleToLongBits(other.tonaz))
			return false;
		if (typLadownosci != other.typLadownosci)
			return false;
		return true;
	}

}
