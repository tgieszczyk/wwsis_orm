package pl.wwsis.joined;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import pl.wwsis.typy.TypMotocykla;

/**
 * @author ORM
 *
 */
@Entity
@Table(name = "JOINED_MOTOCYKL")
public class MotocyklJoined extends PojazdJoined {
	@Enumerated(EnumType.STRING)
	private TypMotocykla typMotocykla;

	public TypMotocykla getTypMotocykla() {
		return typMotocykla;
	}

	public void setTypMotocykla(TypMotocykla typMotocykla) {
		this.typMotocykla = typMotocykla;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((typMotocykla == null) ? 0 : typMotocykla.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MotocyklJoined other = (MotocyklJoined) obj;
		if (typMotocykla != other.typMotocykla)
			return false;
		return true;
	}

}
