package pl.wwsis;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.tableperclass.MotocyklTablePClass;
import pl.wwsis.tableperclass.PojazdTablePClass;
import pl.wwsis.tableperclass.SamochodCiezarowyTablePClass;
import pl.wwsis.tableperclass.SamochodOsoboowyTablePClass;
import pl.wwsis.typy.RodzajSilnika;
import pl.wwsis.typy.TypLadownosci;
import pl.wwsis.typy.TypMotocykla;
import pl.wwsis.typy.TypOsobowy;

public class MainTablePerClass {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();

		// jesteśmy gotowi aby tworzyć encję

		EntityTransaction transaction = em.getTransaction();

		try {
			PojazdTablePClass ciezarowy = createSamochodCiezarowyTablePClass();
			PojazdTablePClass osobowy = createSamochodOsobowyTablePClass();
			PojazdTablePClass motocykl = createMotorTablePClass();
			
			transaction.begin();

			// wykonujemy operacje create, delete, update
			//em.persist(p);
			// em.remove
			// em.merge

			em.persist(motocykl);
			em.persist(osobowy);
			em.persist(ciezarowy);

			transaction.commit();
			
			PojazdTablePClass szukanyPojazd = em.find(PojazdTablePClass.class, motocykl.getId());
			
			if(szukanyPojazd != null && szukanyPojazd instanceof MotocyklTablePClass) {
				obsluzMotocykl((MotocyklTablePClass) szukanyPojazd);
			}
			
			System.exit(0);
		} catch (Exception e) {

			// dodajemy rollback jeżeli pojawi się błąd
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}
	
	private static void obsluzMotocykl(MotocyklTablePClass motocykl) {
		System.out.println();
		// zrob cos z motocyklem
	}
	
	private static PojazdTablePClass createSamochodOsobowyTablePClass() {
		SamochodOsoboowyTablePClass pojazd = new SamochodOsoboowyTablePClass();
		
		pojazd.setKolor("Czerwony");
		pojazd.setLiczbaDrzwi(5);
		pojazd.setMarka("Mazda CX5");
		pojazd.setMksymalnaLiczbaPasazerow(5);
		pojazd.setMocKW(120);
		pojazd.setPojemnosc(2500);
		pojazd.setNmp(400);
		pojazd.setPredkoscMaksynalna(220);
		pojazd.setPrzyspieszenie(12);
		pojazd.setRodzajSilnika(RodzajSilnika.BENZYNA);
		pojazd.setSrednieSpalaniePer100km(6.5);
		pojazd.setTyp(TypOsobowy.CROSSOVER);
		
		return pojazd;
	}
	
	private static PojazdTablePClass createSamochodCiezarowyTablePClass() {
		SamochodCiezarowyTablePClass pojazd = new SamochodCiezarowyTablePClass();
		
		pojazd.setKolor("Czerwony");
		pojazd.setMarka("VOLVO FH4");
		pojazd.setMocKW(120);
		pojazd.setPojemnosc(8500);
		pojazd.setNmp(400);
		pojazd.setPredkoscMaksynalna(150);
		pojazd.setPrzyspieszenie(25);
		pojazd.setRodzajSilnika(RodzajSilnika.DIESEL);
		pojazd.setSrednieSpalaniePer100km(19);
		pojazd.setTonaz(60);
		pojazd.setLadownosc(20000);
		pojazd.setTypLadownosci(TypLadownosci.NACZEPA);
		
		return pojazd;
	}
	
	private static PojazdTablePClass createMotorTablePClass() {
		MotocyklTablePClass pojazd = new MotocyklTablePClass();

		pojazd.setKolor("Czerwony");
		pojazd.setMarka("Suzuki Ninja");
		pojazd.setMocKW(120);
		pojazd.setPojemnosc(500);
		pojazd.setNmp(400);
		pojazd.setPredkoscMaksynalna(280);
		pojazd.setPrzyspieszenie(2);
		pojazd.setRodzajSilnika(RodzajSilnika.BENZYNA);
		pojazd.setSrednieSpalaniePer100km(5);
		pojazd.setTypMotocykla(TypMotocykla.SCIGACZ);
		return pojazd;
	}

}
