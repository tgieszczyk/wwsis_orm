package pl.wwsis.wyklad1_01.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author a051213
 * pokazac jak zapisac obiekt do pliku (serializable)
 */
@Entity
@Table(name = "CUSTOMER")
public class Klient implements Serializable {
	private static final long serialVersionUID = -3414858077480884349L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PK_ID_KLIENT")
	private long id;

	private String nazwa;
	private String ulica;
	private String miasto;
	private String kodPocztowy;
	private String osobaKontaktowa;
	private String numerTelefonu;
	private String email;
	private String nip;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getMiasto() {
		return miasto;
	}

	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}

	public String getKodPocztowy() {
		return kodPocztowy;
	}

	public void setKodPocztowy(String kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}

	public String getOsobaKontaktowa() {
		return osobaKontaktowa;
	}

	public void setOsobaKontaktowa(String osobaKontaktowa) {
		this.osobaKontaktowa = osobaKontaktowa;
	}

	public String getNumerTelefonu() {
		return numerTelefonu;
	}

	public void setNumerTelefonu(String numerTelefonu) {
		this.numerTelefonu = numerTelefonu;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

}
