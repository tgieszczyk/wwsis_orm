package pl.wwsis.wyklad1_00;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pl.wwsis.wyklad1_01.entities.Klient;

public class Main {

	private static final String GLOBAL_ID = "klient.%d.id";
	private static final String GLOBAL_EMAIL = "klient.%d.email";
	public static void main(String[] args) throws Exception {
//		Properties p = new Properties();
//		p.load(Main.class.getResourceAsStream("dane.properties"));
//		
//		for(int i = 1; ;i++) {
//			
//			String kluczId= String.format(GLOBAL_ID, i);
//			String kluczEmail = String.format(GLOBAL_EMAIL, i);
//			
//			String id = (String)p.get(kluczId);
//			if(id == null) {
//				break;
//			}
//			
//			Integer intId = Integer.valueOf(id);
//		}
//		
//		System.out.println(p);
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("orm-unit");
		
		EntityManager em = factory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		
		try {
			transaction.begin();
			
			for( int i=0; i<10; i++) {
				Klient k = new Klient();
				k.setEmail("klient@domena.pl");
				k.setKodPocztowy("52-222");
				k.setMiasto("Siechnice");
				k.setNazwa("WWSIS");
				k.setNip("333-22-44-555");
				k.setNumerTelefonu("ttttttt");
				k.setOsobaKontaktowa("Franek Frankowski");
				k.setUlica("Migawki 34/2");
//				k.setId(1L);
				em.persist(k);
				System.out.println(k.getId());
			}	
			
			transaction.commit();
			System.exit(0);
		} catch(Exception e){
			transaction.rollback();
		} finally {
//			if(transaction.isActive()) {
//				transaction.commit();
//			}
		}
		
		

		
//		Object result = em.find(Klient.class, 1l);
//		System.out.println(result);
//		
//		Klient k1 = (Klient)result;
//		
//		System.out.println(k1.getEmail());
//		k1.setEmail("java-orm@wwsis.com.pl");
//		
//		transaction.begin();
//		em.merge(k1);
//		transaction.commit();
//		
//		
//		transaction.begin();
//		em.remove(k1);
//		transaction.commit();
//		
//		result = em.find(Klient.class, 1l);
//		System.out.println(result);
	}
	

}
