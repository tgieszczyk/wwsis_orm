package pl.wwsis.wyklad2.p02;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class EnumeratedTest {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			Uzytkownik u = new Uzytkownik();
			u.setId(1);
			u.setName("Admin");
			u.setNip("9090909090");
			u.setTypUzytkownika(TypUzytkownika.ADMINISTRATOR);
			u.setAlternatywnyTyp(TypUzytkownika.ADMINISTRATOR);
			em.persist(u);
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
	}

}
