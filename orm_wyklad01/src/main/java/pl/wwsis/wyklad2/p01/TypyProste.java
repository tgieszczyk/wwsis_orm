package pl.wwsis.wyklad2.p01;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TypyProste implements Serializable {

	private static final long serialVersionUID = 7662187375724840L;
	@Id
	private long id;
	private float cena;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getCena() {
		return cena;
	}

	public void setCena(float cena) {
		this.cena = cena;
	}

}
