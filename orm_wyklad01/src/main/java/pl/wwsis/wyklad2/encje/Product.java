package pl.wwsis.wyklad2.encje;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PRODUKT")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name = "NAZWA")
	private String nazwa;
	@Column(name = "OPIS")
	@Lob
	private String opis;
	@Column(name = "CENA")
	private BigDecimal cena;
	@Column(name = "W_OFERCIE")
	private boolean wOfercie;
	@Column(name = "DATA_DODANIA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDodania;
	@Column(name = "OBRAZ")
	@Lob
	@Basic(fetch = FetchType.EAGER)
	private byte[] obraz;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public BigDecimal getCena() {
		return cena;
	}

	public void setCena(BigDecimal cena) {
		this.cena = cena;
	}

	public boolean iswOfercie() {
		return wOfercie;
	}

	public void setwOfercie(boolean wOfercie) {
		this.wOfercie = wOfercie;
	}

	public Date getDataDodania() {
		return dataDodania;
	}

	public void setDataDodania(Date dataDodania) {
		this.dataDodania = dataDodania;
	}

	public byte[] getObraz() {
		return obraz;
	}

	public void setObraz(byte[] obraz) {
		this.obraz = obraz;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cena == null) ? 0 : cena.hashCode());
		result = prime * result
				+ ((dataDodania == null) ? 0 : dataDodania.hashCode());
		result = prime * result + id;
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		result = prime * result + Arrays.hashCode(obraz);
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + (wOfercie ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (cena == null) {
			if (other.cena != null)
				return false;
		} else if (!cena.equals(other.cena))
			return false;
		if (dataDodania == null) {
			if (other.dataDodania != null)
				return false;
		} else if (!dataDodania.equals(other.dataDodania))
			return false;
		if (id != other.id)
			return false;
		if (nazwa == null) {
			if (other.nazwa != null)
				return false;
		} else if (!nazwa.equals(other.nazwa))
			return false;
		if (!Arrays.equals(obraz, other.obraz))
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		if (wOfercie != other.wOfercie)
			return false;
		return true;
	}

	// GENERUJEMY TZW GETTERS I SETTERS ALT+SHIFT+s
	// GENERUJEMY METODY hashCode i equals
}
