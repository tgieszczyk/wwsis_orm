package pl.wwsis.wyklad2.p04;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Zdjecie {
	@Id
	private long id;
	private long idUzytkownika;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	// typ kolumny zdjecie to BLOB
	// FetchType.LAZY - oznacza ze tablica bajt�w pobierana jest z bazy danych
	// podczas bezpo�redniego odwo�ania si� do pola zdjecie
	private byte[] zdjecie;

	@Lob
	@Basic(fetch = FetchType.EAGER)
	// typ kolumny zdjecie to CLOB
	// FetchType.EAGER - oznacza ze ci�g znak�w jest pobierany podczas budowania
	// obiektu encji przy providera
	private String opis;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdUzytkownika() {
		return idUzytkownika;
	}

	public void setIdUzytkownika(long idUzytkownika) {
		this.idUzytkownika = idUzytkownika;
	}

	public byte[] getZdjecie() {
		return zdjecie;
	}

	public void setZdjecie(byte[] zdjecie) {
		this.zdjecie = zdjecie;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

}
