package pl.wwsis.wyklad2.p05;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Uzytkownik2 {
	@Id
	private long id;

	@Embedded
	private DodatkoweInformacje dodatkoweInformacje;

	private String name;
	private String nip;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public DodatkoweInformacje getDodatkoweInformacje() {
		return dodatkoweInformacje;
	}

	public void setDodatkoweInformacje(DodatkoweInformacje dodatkoweInformacje) {
		this.dodatkoweInformacje = dodatkoweInformacje;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

}
