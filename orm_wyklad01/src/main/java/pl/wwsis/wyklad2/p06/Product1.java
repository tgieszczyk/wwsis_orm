package pl.wwsis.wyklad2.p06;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.hibernate.annotations.Type;

@Entity
public class Product1 {
	@Id
	private long id;

	private String nazwa;
	private String opis;
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] zdjecie;

	@Type(type = "pl.wwsis.wyklad2.p06.CustomNumericUserType")
	private CustomNumber cena;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public byte[] getZdjecie() {
		return zdjecie;
	}

	public void setZdjecie(byte[] zdjecie) {
		this.zdjecie = zdjecie;
	}

	public CustomNumber getCena() {
		return cena;
	}

	public void setCena(CustomNumber cena) {
		this.cena = cena;
	}

}
