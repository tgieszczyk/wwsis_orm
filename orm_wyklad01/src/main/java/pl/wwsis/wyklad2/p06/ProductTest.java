package pl.wwsis.wyklad2.p06;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class ProductTest {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();

			Product1 p = new Product1();
			p.setCena(new CustomNumber("45.77"));
			p.setId(1);
			p.setNazwa("Wieszak");
			p.setOpis("Wolno stoj�cy wieszak");

			em.persist(p);
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
	}
}
