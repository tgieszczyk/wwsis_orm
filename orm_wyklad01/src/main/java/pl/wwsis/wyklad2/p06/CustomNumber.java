package pl.wwsis.wyklad2.p06;

import java.math.BigDecimal;

public class CustomNumber extends Number {
	private static final long serialVersionUID = 7922131157120656970L;
	private BigDecimal value;

	public BigDecimal getValue() {
		return value;
	}

	public CustomNumber(BigDecimal value) {
		this.value = value;
	}

	public CustomNumber(int value) {
		this.value = new BigDecimal(value);
	}

	public CustomNumber(float value) {
		this.value = new BigDecimal(value);
	}

	public CustomNumber(double value) {
		this.value = new BigDecimal(value);
	}

	public CustomNumber(long value) {
		this.value = new BigDecimal(value);
	}

	public CustomNumber(String value) {
		this.value = new BigDecimal(value);
	}

	public CustomNumber(char[] value) {
		this.value = new BigDecimal(value);
	}

	@Override
	public int intValue() {
		return value.intValue();
	}

	@Override
	public long longValue() {
		return value.longValue();
	}

	@Override
	public float floatValue() {
		return value.floatValue();
	}

	@Override
	public double doubleValue() {
		return value.doubleValue();
	}

	public boolean equals(Object x) {
		return value.equals(x);
	}

	public int hashCode() {
		return value.hashCode();
	}

}
