package pl.wwsis.wyklad2.p07;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.TableGenerator;

@Embeddable
public class KluczKompozytowyStudent implements Serializable {
	private String nazwa;

	@TableGenerator(name = "table_generator_student", valueColumnName = "NASTEPNA_WARTOSC")
	private String numerIndeksu;

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getNumerIndeksu() {
		return numerIndeksu;
	}

	public void setNumerIndeksu(String numerIndeksu) {
		this.numerIndeksu = numerIndeksu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		result = prime * result + ((numerIndeksu == null) ? 0 : numerIndeksu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KluczKompozytowyStudent other = (KluczKompozytowyStudent) obj;
		if (nazwa == null) {
			if (other.nazwa != null)
				return false;
		} else if (!nazwa.equals(other.nazwa))
			return false;
		if (numerIndeksu == null) {
			if (other.numerIndeksu != null)
				return false;
		} else if (!numerIndeksu.equals(other.numerIndeksu))
			return false;
		return true;
	}

}
