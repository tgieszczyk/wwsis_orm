package pl.wwsis.wyklad2.p03;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TemporalTest {
	private transient String value;

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit");

		EntityManager em = factory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			
			DaneOsobowe dane = new DaneOsobowe();
			dane.setCzasUrodzenia(new Date());
			dane.setDataOstatniejModyfikacji(new Date());
			dane.setDataUrodzenia(new Date());
			dane.setId(1);
			dane.setImie("Jan");
			dane.setNazwisko("Kowalski");
			
			em.persist(dane);
		} finally {
			if (transaction.isActive()) {
				transaction.commit();
			}
		}
	}

}
