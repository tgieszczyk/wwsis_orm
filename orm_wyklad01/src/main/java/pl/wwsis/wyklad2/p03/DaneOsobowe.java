package pl.wwsis.wyklad2.p03;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DaneOsobowe {
	@Id
	private long id;

	@Temporal(TemporalType.DATE)
	// DATE - okre�la, �e dane pole b�dzie w bazie danych kolumn� o typie Date
	// (sama data)
	private java.util.Date dataUrodzenia;

	@Temporal(TemporalType.TIMESTAMP)
	// TIMESTAMP - (domy�lny) typ kolumny w bazie danych to timestamp (data i
	// czas)
	private java.util.Date dataOstatniejModyfikacji;

	@Temporal(TemporalType.TIME)
	// TIME - typ kolumy w bazie danych to time (sam czas)
	private java.util.Date czasUrodzenia;

	private String imie;
	private String nazwisko;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public java.util.Date getDataUrodzenia() {
		return dataUrodzenia;
	}

	public void setDataUrodzenia(java.util.Date dataUrodzenia) {
		this.dataUrodzenia = dataUrodzenia;
	}

	public java.util.Date getDataOstatniejModyfikacji() {
		return dataOstatniejModyfikacji;
	}

	public void setDataOstatniejModyfikacji(
			java.util.Date dataOstatniejModyfikacji) {
		this.dataOstatniejModyfikacji = dataOstatniejModyfikacji;
	}

	public java.util.Date getCzasUrodzenia() {
		return czasUrodzenia;
	}

	public void setCzasUrodzenia(java.util.Date czasUrodzenia) {
		this.czasUrodzenia = czasUrodzenia;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

}
